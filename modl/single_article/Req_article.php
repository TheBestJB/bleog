<?php
/**
 * 
 * Classe de requête d'article
 *
 */
class Req_article
{
	/**
	 * @var id article
	 */
	protected $id;
	
	/**
	 * @var id membre
	 */
	protected $id_member;
	
	/**
	 * @var id auteur
	 */
	protected $id_author;
	
	/**
	 * @var Titre
	 */
	protected $title;
	
	/**
	 * @var Description
	 */
	protected $bodytext;
	
	/**
	 * @var Date de publication
	 */
	protected $reg_date;
	
	/**
	 * @var Prénom de l'auteur
	 */
	protected $author_name;
	
	/**
	 * Vérifie si l'id existe en db
	 * 
	 * @param int $id
	 */
	public function verifId($id)
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$conx = $manager->getDB();
		$req = $conx->prepare('SELECT id FROM mygen_articles WHERE id = :id');
		$req->execute(array(':id' => $id));
		$result = $req->fetch(PDO::FETCH_ASSOC);
		$valid = $result ? true : false;
		if ($valid)
		{
			$selArt = $conx->prepare('SELECT id, id_member, id_author, title, bodytext, reg_date FROM mygen_articles WHERE id = :id');
			$selArt->execute(array(':id' => $id));
			$data = $selArt->fetch(PDO::FETCH_ASSOC);
			foreach ($data as $key => $value)
			{
				$method = 'set'.ucfirst($key);
				$this->$method($value);
			}
			// Requête pour prénom de l'auteur
			$selPrenom = $conx->prepare('SELECT mygen_members.prenom
					FROM mygen_members
					INNER JOIN mygen_articles
					ON mygen_members.id = mygen_articles.id_author
					WHERE mygen_articles.id = :id');
			$selPrenom->execute(array(':id' => $id));
			$author = $selPrenom->fetch(PDO::FETCH_ASSOC);
			$this->setAuthor($author['prenom']);
		}
		return $valid;
	}
	
	/**
	 * Setter $this->id
	 * 
	 * @param $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	 * Setter $this->id_member
	 * 
	 * @param $id_member
	 */
	public function setId_member($id_member)
	{
		$this->id_member = $id_member;
	}
	
	/**
	 * Setter $this->id_author
	 * 
	 * @param $id_author
	 */
	public function setId_author($id_author)
	{
		$this->id_author = $id_author;
	}
	
	/**
	 * Setter $this->title
	 * 
	 * @param $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * Setter $this->bodytext
	 * 
	 * @param $bodytext
	 */
	public function setBodytext($bodytext)
	{
		$this->bodytext = $bodytext;
	}
	
	/**
	 * Setter $this->reg_date
	 * 
	 * @param $reg_date
	 */
	public function setReg_date($reg_date)
	{
		$date = date('d/m/Y', strtotime($reg_date));
		$this->reg_date = $date;
	}
	
	/**
	 * Setter $this->author_name
	 * 
	 * @param $author
	 */
	public function setAuthor($author)
	{
		$this->author_name = $author;
	}
	
	// GETTERS
	
	/**
	 * @return $this->id
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @return $this->id_member
	 */
	public function getId_member()
	{
		return $this->id_member;
	}
	
	/**
	 * @return $this->id_author
	 */
	public function getId_author()
	{
		return $this->id_author;
	}
	
	/**
	 * @return $this->title
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * @return $this->bodytext
	 */
	public function getBodytext()
	{
		return $this->bodytext;
	}
	
	/**
	 * @return $this->reg_date
	 */
	public function getReg_date()
	{
		return $this->reg_date;
	}
	
	/**
	 * @return $this->author_name
	 */
	public function getAuthor()
	{
		return $this->author_name;
	}
}