<?php
if (isset($_SESSION['newsletter_error_title']))
{
	$err_title = '<small><font color="red"><b> Veuillez entrer un titre !</b></font></small>';
	unset($_SESSION['newsletter_error_title']);
}
else
{
	$err_title = null;
}
if (isset($_SESSION['newsletter_error_desc']))
{
	$err_desc = '<small><font color="red"><b> Veuillez entrer un texte !</b></font></small>';
	unset($_SESSION['newsletter_error_desc']);
}
else
{
	$err_desc = null;
}
if (isset($_SESSION['news_title']))
{
	$news_title = 'value="'.$_SESSION['news_title'].'"';
	unset($_SESSION['news_title']);
}
else
{
	$news_title = null;
}
if (isset($_SESSION['news_desc']))
{
	$news_desc = $_SESSION['news_desc'];
	unset($_SESSION['news_desc']);
}
else
{
	$news_desc = null;
}
?>
<form method="post" action="view/newsletter/sendNewsletter.php" enctype="multipart/form-data">
  <div class="form-group">
    <label for="articles_title">Titre de la newsletter (objet du mail) :</label><?= $err_title; ?>
    <input type="text" class="form-control" id="newsletters_title" placeholder="Veuillez entrer un titre" <?= $news_title; ?> name="newsletters_title">
  </div>
  <div class="form-group">
    <label for="newsletters_body">Message :</label><?= $err_desc; ?>
    <textarea class="form-control" id="newsletters_body" placeholder="Tapez le message" rows="4" name="newsletters_message"><?= $news_desc; ?></textarea>
  </div>
  <button type="submit" class="btn btn-primary" name="newsletter_sending">Envoyer</button>
</form>
<br>
<form action="view/newsletter/mailsList.php" method="post" target="_blank">
	<button type="submit" class="btn btn-success" name="mailsList">Listes des inscrits</button>
</form>
<script>CKEDITOR.replace('newsletters_message', {
	filebrowserBrowseUrl: '/articles_pics/sel_pics.php',
    filebrowserUploadUrl: '/articles_pics/upload.php'
});</script>