<?php
class UserManagerPDO
{
	protected $db;
	
	public function __construct(PDO $db)
	{
		$this->db = $db;
	}
	
	/**
	 * Méthode de log user
	 * 
	 * @param string $login
	 * @param string $pass
	 * @param string $mode : 'classic' or 'cookie' connexion
	 * @return $user
	 */
	public function login($login, $pass, $mode)
	{
		if (!empty($login) && !empty($pass))
		{
			if ($mode === 'classic')
			{
				$pass = sha1($pass);
			}
			elseif ($mode === 'cookie')
			{
				$pass = $pass;
			}
			$req = $this->db->prepare('SELECT id, pseudo, nom, prenom, password, email, registration_date, edit_rights FROM mygen_members WHERE pseudo = :pseudo AND password = :password');
			$req->execute([':pseudo' => $login, ':password' => $pass]);
			
			$req->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'UserManager');
			$user = $req->fetch();
			
			return $user;
		}
	}
	
	/**
	 * @return connexion DB
	 */
	public function getDB()
	{
		return $this->db;
	}
}