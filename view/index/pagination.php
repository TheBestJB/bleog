<?php
if ($max > 1)
{
?>
<nav style="text-align:center;">
  <ul class="pagination">
    <li>
<?php 
if ($_GET['p'] < $max)
{
	$next = $_GET['p'] < $max ? ($_GET['p'] + 1) : $max;
?>
	    <a href="<?= 'Accueil.php?p='.$next ?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span> Remonter dans le temps
        </a>
<?php 
}
?>
<?php 
if ($_GET['p'] > 1)
{
	$prev = $_GET['p'] > $max ? $max : ($_GET['p'] - 1);
?>
        <a href="<?= 'Accueil.php?p='.$prev ?>" aria-label="Next">
	    Avancer dans le temps <span aria-hidden="true">&raquo;</span>
	    </a>
<?php
}
?>
    </li>
  </ul>
</nav>
<?php
}
?>