<?php
class View_photo
{
	protected $id;
	protected $id_member;
	protected $picsname;
	protected $reg_date;
	protected $monthyear;
	protected $form_date;
	protected $description;
	
	public function __construct($array)
	{
	if (!empty($array))
		{
			foreach ($array as $key => $value)
			{
				$method = 'set'.ucfirst($key);
				$this->$method($value);
			}
			$this->setView();
		}
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function setId_member($id_member)
	{
		$this->id_member = $id_member;
	}
	
	public function setPicsname($picsname)
	{
		$this->picsname = $picsname;
	}
	
	public function setMonthyear($monthyear)
	{
		$this->monthyear = $monthyear;
	}
	
	public function setReg_date($reg_date)
	{
		$this->reg_date = $reg_date;
		$date = date('d/m/Y', strtotime($reg_date));
		$this->form_date = $date;
		
	}
	
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	public function setView()
	{
		include 'view/single_photo/view.php';
	}
}