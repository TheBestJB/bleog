<?php
class Req_photo
{
	public function __construct($id)
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$rdb = $manager->getDB();
		$req = $rdb->prepare('SELECT id, id_member, picsname, reg_date, description FROM mygen_photos WHERE id = :id');
		$req->execute(array(':id' => $id));
		
		while ($data = $req->fetch(PDO::FETCH_ASSOC))
		{
			include 'view/single_photo/View_photo.php';
			new View_photo($data);
		}
	}
}