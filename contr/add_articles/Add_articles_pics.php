<?php
require '../lib/Blog_treatments.php';
require '../modl/add_articles/add_articles_pics_DB.php';

/**
 * Classe demandant au modèle l'ajout d'une photo en db.
 */
class Add_articles_pics extends Blog_treatments
{
	/**
	 * Prend en paramètre $_FILE et vérifie la validité du fichier.
	 * 
	 * @param array $files
	 */
	public function __construct($files)
	{
		$vpic = $this->valid_articles_pics($files);
		if (!$vpic)
		{
			echo 'Fichier incorrect !';
		}
		else
		{
			$addPics = new add_articles_pics_DB($this->rand_picsname);
			if (!$addPics)
			{
				echo 'Error !';
			}
			else
			{
				echo 'Photo ajoutée avec succès !';
			}
		}
	}
}