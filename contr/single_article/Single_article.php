<?php
/**
 * 
 * Classe de contrôle de l'id article
 *
 */
class Single_article
{
	/**
	 * Constructeur
	 * 
	 * @param int $id
	 */
	public function __construct($id)
	{
		$id = (int) $id;
		if ($id > 0)
		{
			require 'modl/single_article/Req_article.php';
			$vart = new Req_article();
			if ($vart->verifId($id))
			{
				include 'view/single_article/view.php';
			}
			else
			{
				echo '<h2 style="text-align: center;">Désolé, cette page n\'existe pas !</h2>';
			}
		}
		else
		{
			echo '<h2 style="text-align: center;">Désolé, cette page n\'existe pas !</h2>';
		}
	}
}