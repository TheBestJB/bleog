<?php 
if (isset($_SESSION['error_log']) && $_SESSION['error_log'] == 1)
{
	echo '<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Connexion refusée !</strong> Identifiants incorrects, veuillez réessayer.</div>';
}
else 
{
	$_SESSION['error_log'] = null;
}
?>
<form class="form-signin" action="index.php" method="post">
	<h2 class="form-signin-heading">Connectez-vous</h2>
	<label for="inputPseudo" class="sr-only">Pseudo</label>
	<input type="text" id="inputPseudo" name="inputPseudo" class="form-control" placeholder="Pseudo" required autofocus>
	<label for="inputPassword" class="sr-only">Mote de passe</label>
	<input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Mot de passe" required>
	
	<div class="checkbox">
		<label>
		<input type="checkbox" value="remember-me" name="remember-me"> Se souvenir de moi
		</label>
	</div>
	
	<button class="btn btn-lg btn-primary btn-block" type="submit" name="connexion">Connexion</button>
</form>