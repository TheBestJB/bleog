<?php
session_start();
if ($_SESSION['edit_rights'] == 1 && isset($_GET['id']))
{
	include '../../contr/edit_articles/Edit_articles.php';
	$id = (int) $_GET['id'];
	new Edit_articles($id, $_POST);
}
else
{
	header('Location: ../../index.php');
}