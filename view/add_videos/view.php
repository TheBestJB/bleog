<?php
if (isset($_SESSION['video_error_date']))
{
	$err_date = '<small><font color="red"><b> Veuillez entrer une date valide !</b></font></small>';
	unset($_SESSION['video_error_date']);
}
else
{
	$err_date = null;
}
if (isset($_SESSION['video_error_title']))
{
	$err_title = '<small><font color="red"><b> Veuillez entrer un titre !</b></font></small>';
	unset($_SESSION['video_error_title']);
}
else
{
	$err_title = null;
}
if (isset($_SESSION['video_error_url']))
{
	$err_url = '<small><font color="red"><b> Veuillez entrer un lien youtube !</b></font></small>';
	unset($_SESSION['video_error_url']);
}
else
{
	$err_url = null;
}
if (isset($_SESSION['error_link']))
{
	$err_link = '<small><font color="red"><b> Le lien youtube n\'est pas valide !</b></font></small>';
	$bad_url = 'value="'.$_SESSION['bad_link'].'"';
	unset($_SESSION['error_link']);
	unset($_SESSION['bad_link']);
}
else
{
	$err_link = null;
	$bad_url = null;
	$_SESSION['bad_link'] = null;
	
}
if (isset($_SESSION['vid_title']))
{
	$vid_title = 'value="'.$_SESSION['vid_title'].'"';
	unset($_SESSION['vid_title']);
}
else
{
	$vid_title = null;
}
if (isset($_SESSION['vid_url']))
{
	$vid_url = 'value="'.$_SESSION['vid_url'].'"';
	unset($_SESSION['vid_url']);
}
else
{
	$vid_url = null;
}
?>
<form method="post" action="view/add_videos/upload.php" enctype="multipart/form-data">
  <div class="form-group">
    <label for="videos_date">Date de la vidéo (jj/mm/aaaa)</label><?= $err_date; ?>
    <input type="date" class="form-control" id="videos_date" placeholder="jj-mm-aaaa" name="videos_real_date">
  </div>
  <div class="form-group">
    <label for="videos_title">Titre de la vidéo :</label><?= $err_title; ?>
    <input type="text" class="form-control" id="videos_title" placeholder="Veuillez entrer un titre" <?= $vid_title ?> name="videos_title">
  </div>
  <div class="form-group">
    <label for="videos_url">Lien de la vidéo :</label><?= $err_url ?><?= $err_link ?>
    <input type="url" class="form-control" id="videos_url" placeholder="Entrez un lien youtube" <?= $vid_url ?><?= $bad_url ?> name="videos_url">
  </div>
  <button type="submit" class="btn btn-primary" name="video_sending">Envoyer</button>
</form>