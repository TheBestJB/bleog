<?php
require_once '../../modl/DBFactory.php';
require_once '../../modl/UserManagerPDO.php';

/**
 * 
 * Classe d'ajout d'article en db
 *
 */
class add_aticles_DB
{
	/**
	 * Constructeur
	 * 
	 * @param date $date
	 * @param string $title
	 * @param string $description
	 * @return bool
	 */
	public function __construct($date, $title, $description)
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$conx = $manager->getDB();
		$req = $conx->prepare('INSERT INTO mygen_articles(id_member, id_author, title, bodytext, type, reg_date) VALUES(:id_member, :id_author, :title, :bodytext, :type, :reg_date)');
		$req->execute(array(
				':id_member' => 1,
				':id_author' => $_SESSION['id'],
				':title' => $title,
				':bodytext' => $description,
				':type' => 'article',
				':reg_date' => $date
		));
		return true;
	}
}