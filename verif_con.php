<?php
if (isset($_POST['inputPseudo']) && isset($_POST['inputPassword']) && isset($_POST['connexion']))
{
	require_once 'modl/UserManager.php';
	require_once 'modl/UserManagerPDO.php';
	require_once 'modl/DBFactory.php';
	$db = DBFactory::getPDOCon();
	$manager = new UserManagerPDO($db);
	$user = $manager->login(htmlspecialchars($_POST['inputPseudo']), htmlspecialchars($_POST['inputPassword']));
	if ($user)
	{
		require_once 'view/index/index_logged.php';
	}
	else
	{
		header('Location:'.$_SERVER['REQUEST_URI']);
		echo 'fail';
	}
}