<?php
class Month
{
	/**
	 * @var PDO $rdb
	 */
	protected $rdb;
	protected $nbPages;
	
	public function __construct($id, $page)
	{
		
		// Calcul du LIMIT selon $page
		$nb = ($page - 1) * 6;
		
		//Count nb pages
		$this->countNbPages();
		
		//inclusion pagination
		$max = $this->getNbPages();
		include 'view/index/pagination.php';
		
		$db = DBFactory::getPDOCon();
		$condb = new UserManagerPDO($db);
		$rdb = $condb->getDB();
		$this->setRdb($rdb);
		$req = $this->rdb->prepare('
				SELECT EXTRACT(YEAR_MONTH FROM reg_date) AS short_date FROM mygen_photos WHERE id_member = :id
				UNION
				SELECT EXTRACT(YEAR_MONTH FROM reg_date) AS short_date FROM mygen_articles WHERE id_member = :id
				UNION
				SELECT EXTRACT(YEAR_MONTH FROM reg_date) AS short_date FROM mygen_videos WHERE id_member = 1 ORDER BY short_date DESC LIMIT :nb, 6
				');
		$req->bindValue(':id', $id, PDO::PARAM_INT);
		$req->bindValue(':id', $id, PDO::PARAM_INT);
		$req->bindValue(':id', $id, PDO::PARAM_INT);
		$req->bindValue(':nb', $nb, PDO::PARAM_INT);
		$req->execute();
		setlocale(LC_TIME, 'fr_FR');
		include 'view/monthly/photos/PhotosView.php';
		include 'view/monthly/articles/ArticlesView.php';
		include 'view/monthly/videos/VideosView.php';
		
		while ($data = $req->fetch(PDO::FETCH_ASSOC))
		{
			$tmp_date = strtotime($data['short_date'].'15');
			$MYDate = strftime('%B %Y', $tmp_date);
			echo '<div class="panel panel-primary-green"><div class="panel-heading-green"><h3 class="panel-title"><span class="glyphicon glyphicon-circle-arrow-right"></span><b> '.utf8_encode(ucfirst($MYDate)).'</b></h3></div>
					<div class="panel-body">';
			$this->selAll($data);
			echo '</div></div>';
		}
		
		//inclusion pagination
		include 'view/index/pagination.php';
		
	}
	
	/**
	 * Connexion à la db
	 * 
	 * @param PDO $rdb
	 */
	public function setRdb($rdb)
	{
		$this->rdb = $rdb;
	}
	
	/**
	 * Sélectionne les photos mensuellement
	 * 
	 * @param array $data
	 */
	public function selPhotos($data)
	{
		$reqByMonth = $this->rdb->prepare('SELECT id, id_member, picsname, reg_date, description FROM mygen_photos WHERE id = :id');
		$reqByMonth->execute(array(':id' => $data));
		$photo = $reqByMonth->fetch(PDO::FETCH_ASSOC);
		new PhotosView($photo);
	}
	
	/**
	 * Sélectionne les articles mensuellement
	 * 
	 * @param array $data
	 */
	public function selArticles($data)
	{
		$reqByMonth = $this->rdb->prepare('SELECT id, id_member, id_author, title, bodytext, reg_date FROM mygen_articles WHERE id = :id');
		$reqByMonth->execute(array(':id' => $data));
		$article = $reqByMonth->fetch(PDO::FETCH_ASSOC);
		new ArticlesView($article);
	}
	
	/**
	 * Sélectionne les vidéos mensuellement
	 *
	 * @param array $data
	 */
	public function selVideos($data)
	{
		$reqByMonth = $this->rdb->prepare('SELECT id, id_member, url_title, url_link, reg_date FROM mygen_videos WHERE id = :id');
		$reqByMonth->execute(array(':id' => $data));
		$video = $reqByMonth->fetch(PDO::FETCH_ASSOC);
		new VideosView($video);
	}
	
	/**
	 * Sélectionne toutes les photos, vidéos, articles du mois donné.
	 * 
	 * @param array $data
	 */
	public function selAll(array $data)
	{
		$reqByMonth = $this->rdb->prepare('SELECT picsname AS main_col, id, reg_date, type FROM mygen_photos WHERE EXTRACT(YEAR_MONTH FROM reg_date) = :date
			UNION
			SELECT title, id, reg_date, type FROM mygen_articles WHERE EXTRACT(YEAR_MONTH FROM reg_date) = :date
			UNION 
			SELECT url_title, id, reg_date, type FROM mygen_videos WHERE EXTRACT(YEAR_MONTH FROM reg_date) = :date
			ORDER BY reg_date DESC');
		$reqByMonth->execute(array(':date' => $data['short_date']));
		
		while ($data2 = $reqByMonth->fetch(PDO::FETCH_ASSOC))
		{
			if ($data2['type'] == 'photo')
			{
				$this->selPhotos($data2['id']);
			}
			elseif ($data2['type'] == 'article')
			{
				$this->selArticles($data2['id']);
			}
			elseif ($data2['type'] == 'video')
			{
				$this->selVideos($data2['id']);
			}
		}
	}
	
	public function countNbPages() {
		$db = DBFactory::getPDOCon();
		$condb = new UserManagerPDO($db);
		$rdb = $condb->getDB();
		$this->setRdb($rdb);
		$req = $this->rdb->prepare('
				SELECT EXTRACT(YEAR_MONTH FROM reg_date) AS short_date FROM mygen_photos WHERE id_member = 1
				UNION
				SELECT EXTRACT(YEAR_MONTH FROM reg_date) AS short_date FROM mygen_articles WHERE id_member = 1
				UNION
				SELECT EXTRACT(YEAR_MONTH FROM reg_date) AS short_date FROM mygen_videos WHERE id_member = 1 ORDER BY short_date DESC
				');
		$req->execute();
		$nbPages = count($req->fetchAll());
		$this->setNbPages($nbPages);
	}
	
	public function getNbPages() {
		return $this->nbPages;
	}
	public function setNbPages($nbPages) {
		$this->nbPages = ceil($nbPages/6);
		return $this;
	}
	
}