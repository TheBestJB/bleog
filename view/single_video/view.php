<?php
if (isset($_SESSION['modifVTitle_Success']))
{
	$wellDone = 'Le titre a été modifié avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifVTitle_Success']);
}
if (isset($_SESSION['error_Vdate']))
{
	$notDone = 'Le format de la date est incorrect !';
	include 'view/error_alert.php';
	unset($_SESSION['error_Vdate']);
}
if (isset($_SESSION['error_Vurl']))
{
	$notDone = 'Le lien n\'est pas valide !';
	include 'view/error_alert.php';
	unset($_SESSION['error_Vurl']);
}
if (isset($_SESSION['modifVDate_Success']))
{
	$wellDone = 'La date a été modifiée avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifVDate_Success']);
}
if (isset($_SESSION['modifUrl_Success']))
{
	$wellDone = 'La vidéo a été modifié avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifUrl_Success']);
}
?>
<h2 style="text-align: center;">Vidéo du <?= $vvid->getReg_date(); ?></h2>
<hr>
<!-- Bouton Edit -->
<?php
if (isset($_SESSION['edit_rights']) && $_SESSION['edit_rights'] == 1)
{
?>
<div class="btn-group" style="margin-bottom: 20px;">
  <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="glyphicon glyphicon-edit"></span> Modifier <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="#" data-toggle="modal" data-target="#dateEdit"><span class="glyphicon glyphicon-calendar"></span> Modifier la date</a></li>
    <li><a href="#" data-toggle="modal" data-target="#titleEdit"><span class="glyphicon glyphicon-align-left"></span> Modifier le titre</a></li>
    <li><a href="#" data-toggle="modal" data-target="#urlEdit"><span class="glyphicon glyphicon-wrench"></span> Modifier la vidéo</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#removeAll"><span class="glyphicon glyphicon-trash"></span> Supprimer la page</a></li>
  </ul>
</div>
<!-- Fin Bouton Edit -->
<!-- Modals -->
<div class="modal fade" id="dateEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modification de la date</h4>
      </div>
      <form name="editDate" class="form-inline" action="view/single_video/edit_videos.php?id=<?= $_GET['videoId']; ?>" method="post">
      <div class="modal-body">
        La date actuelle est : <?= $vvid->getReg_date(); ?>
        <br /><br />
        	<div class="form-group">
        		<label for="videos_date">Modifier :</label> <input type="date" class="form-control" id="videos_date" placeholder="jj-mm-aaaa" name="real_date">
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" name="sendEditDate" class="btn btn-primary">Sauvegarder</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="titleEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modification du titre</h4>
      </div>
      <form name="editTitle" action="view/single_video/edit_videos.php?id=<?= $_GET['videoId']; ?>" method="post">
      <div class="modal-body">
      	<div class="form-group">
        	<label for="title">Titre :</label><input type="text" class="form-control" id="title" value="<?= $vvid->getUrl_title(); ?>" name="title">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" name="sendEditTitle" class="btn btn-primary">Sauvegarder</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="urlEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modification du lien de la vidéo</h4>
      </div>
      <form name="editUrl" action="view/single_video/edit_videos.php?id=<?= $_GET['videoId']; ?>" method="post">
      <div class="modal-body">
      	<div class="form-group">
        	<label for="title">Nouveau lien :</label><input type="url" class="form-control" id="url_link" name="url_link">
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" name="sendEditUrl" class="btn btn-primary">Sauvegarder</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="removeAll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Supprimer la page</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" role="alert"><b>Attention !</b> Êtes-vous sûr de vouloir supprimer définitivement cette page ?</div>
      </div>
      <form name="supprPage" action="view/single_video/edit_videos.php?id=<?= $_GET['videoId']; ?>" method="post">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-danger" name="sendSuppr">Confirmer la suppression</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- End modals -->
<?php
}
?>
<div class="panel panel-default">
	<div class="panel-heading"><h3 style="text-align: center; font-weight: 800;"><?= $vvid->getUrl_title(); ?></h3></div>
<div class="panel-body">
	<div class="embed-responsive embed-responsive-16by9">
		<iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/<?= $vvid->getUrl_link(); ?>" allowfullscreen></iframe>
	</div>
</div>
</div>