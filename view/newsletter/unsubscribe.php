<?php
session_start();
if ($_POST['sendDel'] && isset($_SESSION['delMail']))
{
	$id = htmlspecialchars($_SESSION['delMail']);
	require_once '../../contr/newsletter/Newsletter.php';
	$unsubscribe = new Newsletter();
	$go = $unsubscribe->deleteMail($id);
	$_SESSION['returnInfoMail'] = $go;
	header('Location: ../../unsubscribe_newsletter.php');
}
else
{
	header('Location: ../../index.php');
}