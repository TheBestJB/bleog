<?php
require_once '../../lib/Blog_treatments.php';
require_once '../../modl/edit_photos/edit_photos_DB.php';
/**
 * class Edit_photos
 */
class Edit_photos extends Blog_treatments
{
	/**
	 * @var int $id
	 */
	protected $id;
	
	/**
	 * Constructeur
	 * 
	 * @param int $id
	 * @param array $post
	 * @param array $files
	 */
	public function __construct($id, $post = array(), $files = array())
	{
		$this->setId($id);
		if ($this->id > 0)
		{
			// Validation et modification de la date
			if (isset($post['sendEditDate']))
			{
				$vdate = $this->valid_date($post['real_date']);
				if ($vdate)
				{
					$this->modifDate($this->id, $post['real_date']);
				}
				else
				{
					session_start();
					$_SESSION['error_date'] = true;
					header('Location: '.$_SERVER['HTTP_REFERER']);
				}
			}
			// Modification de la description
			elseif (isset($post['sendEditDescription']))
			{
				if (!empty($post['description']))
				{
					$this->modifDesc($this->id, $post['description']);
				}
				else
				{
					header('Location: '.$_SERVER['HTTP_REFERER']);
				}
			}
			// Suppression de la description
			elseif (isset($post['supprDesc']))
			{
				$this->supprDesc($this->id);
			}
			// Validation et modification de la photo
			elseif (isset($post['sendEditPhoto']))
			{
				$vphoto = $this->valid_photo($files);
				if ($vphoto)
				{
					$this->modifPhoto($this->id, $this->rand_picsname);
				}
				else
				{
					session_start();
					$_SESSION['error_photo'] = true;
					header('Location: '.$_SERVER['HTTP_REFERER']);
				}
				
			}
			// Suppression de la page
			elseif (isset($post['sendSuppr']))
			{
				$this->supprPage($this->id);
			}
			else
			{
				header('Location: ../../index.php');
			}
		}
		else
		{
			header('Location: ../../index.php');
		}
	}
	
	/**
	 * Setter de l'attribut $id
	 * 
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = (int) $id;
	}
	
	/**
	 * Donne l'ordre au modèle de supprimer la description N°id
	 * 
	 * @param int $id
	 */
	public function supprDesc($id)
	{
		session_start();
		
		$obj = new edit_photos_DB();
		$obj->suppDescr($id);
		$_SESSION['supprDesc_Success'] = true;
		header('Location: '.$_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Envoie nouvelle description au modèle
	 * 
	 * @param int $id
	 * @param string $desc
	 */
	public function modifDesc($id, $desc)
	{
		session_start();
		
		$obj = new edit_photos_DB();
		$desc = htmlspecialchars($desc);
		$obj->modifDesc($id, $desc);
		$_SESSION['modifDesc_Success'] = true;
		header('Location: '.$_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Envoie param nouvelle photo au modèle
	 * 
	 * @param int $id
	 * @param string $pictures_name
	 */
	public function modifPhoto($id, $pictures_name)
	{
		session_start();
		
		$obj = new edit_photos_DB();
		$obj->modifPhoto($id, $pictures_name);
		$_SESSION['modifPhoto_Success'] = true;
		header('Location: '.$_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Envoie nouvelle date au modèle
	 * 
	 * @param int $id
	 * @param date $date
	 */
	public function modifDate($id, $date)
	{
		session_start();
		
		$obj = new edit_photos_DB();
		$obj->modifDate($id, $date);
		$_SESSION['modifDate_Success'] = true;
		header('Location: '.$_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Demande au modèle de supprimer la page
	 * 
	 * @param int $id
	 */
	public function supprPage($id)
	{
		session_start();
		
		$obj = new edit_photos_DB();
		$obj->supprPage($id);
		$_SESSION['supprPage_Success'] = true;
		header('Location: ../../index.php');
	}
}