<?php
require_once '../../lib/Blog_treatments.php';

class Add_photos extends Blog_treatments
{
	public function __construct($post, $files)
	{
		$test_date = $this->valid_date($post['real_date']);
		$vphoto = $this->valid_photo($files);
		if (!$post['real_date'] || !$test_date)
		{
			session_start();
			$_SESSION['err_date'] = 1;
			if (!empty($post['description']))
			{
				$_SESSION['description'] = htmlspecialchars($post['description']);
			}
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		elseif (!$vphoto)
		{
			session_start();
			$_SESSION['no_file'] = 1;
			if (!empty($post['description']))
			{
				$_SESSION['description'] = htmlspecialchars($post['description']);
			}
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		else
		{
			include '../../modl/add_photos/Add_photos_DB.php';
			new Add_photos_DB($post, $this->rand_picsname);
		}
	}
}