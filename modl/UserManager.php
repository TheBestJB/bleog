<?php
class UserManager
{
	protected $id;
	protected $pseudo;
	protected $prenom;
	protected $nom;
	protected $password;
	protected $email;
	protected $registration_date;
	protected $edit_rights;
	
	public function __construct($dataDB = ['col'])
	{
		if (!empty($dataDB))
		{
			$this->hydrate($dataDB);
		}
	}
	
	public function hydrate($dataMySQL)
	{
		foreach ($dataMySQL as $attr => $value)
		{
			$method = 'set'.ucfirst($attr);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}
	
	// SETTERS //
	public function setId($id)
	{
		$this->id = (int) $id;
	}
	
	public function setPseudo($pseudo)
	{
		$this->pseudo = $pseudo;
	}
	
	public function setPrenom($firstname)
	{
		$this->prenom = $firstname;
	}
	
	public function setNom($lastname)
	{
		$this->nom = $lastname;
	}
	
	public function setPassword($password)
	{
		$this->password = $password;
	}
	
	public function setEmail($email)
	{
		$this->email = $email;
	}
	
	public function setRegistration_date($registration)
	{
		$this->registration_date = $registration;
	}
	
	public function setEdit_rights($edit_rights)
	{
		$this->edit_rights = $edit_rights;
	}
	
	// GETTERS //
	public function getId()
	{
		return $this->id;
	}
	
	public function getPrenom()
	{
		return $this->prenom;
	}
	
	public function getNom()
	{
		return $this->nom;
	}
	
	public function getPassword()
	{
		return $this->password;
	}
	
	public function getPseudo()
	{
		return $this->pseudo;
	}
	
	public function getEmail()
	{
		return $this->email;
	}
	
	public function getRegistration_date()
	{
		return $this->registration_date;
	}
	
	public function getEdit_rights()
	{
		return $this->edit_rights;
	}
}