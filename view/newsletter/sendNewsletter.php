<?php
session_start();
if (!$_POST)
{
	header('Location: ../../index.php');
}
else
{
	require_once '../../contr/newsletter/Newsletter.php';
	$sendNewsletter = new Newsletter();
	$sendNewsletter->sendToAllSubscribers($_POST);
}