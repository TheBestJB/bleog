/**
 * Script envoyant les données postées de la newsletter.
 */
$(function() {
	$('#sub').submit(function(e) {
		$.post('contr/newsletter/load_newsletter.php',{ post: $('#mail').val() },
				function(data) {
					$('#emldiv').replaceWith(data);
		});
		e.preventDefault();
	});
});