<?php
require '../modl/DBFactory.php';
require '../modl/UserManagerPDO.php';

class add_articles_pics_DB
{
	public function __construct($picsname)
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$conx = $manager->getDB();
		$req = $conx->prepare('INSERT INTO mygen_articles_pics(id_member, picsname, reg_date) VALUES(:id_member, :picsname, NOW())');
		$req->execute(array(
				':id_member' => 1,
				':picsname' => $picsname
		));
		return true;
	}
}