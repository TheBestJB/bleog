<?php
session_start();
function loadClass($className)
{
	include 'contr/'.strtolower($className).'/'.$className.'.php';
}
spl_autoload_register('loadClass');
if ($_SESSION['edit_rights'] == 1)
{
	require_once 'view/add_photos/add_photos_logged.php';
}
else
{
	header('Location: index.php');
}