<?php
session_start();
function loadClass($className)
{
	include 'contr/'.strtolower($className).'/'.$className.'.php';
}
spl_autoload_register('loadClass');

require_once 'modl/UserManager.php';
require_once 'modl/UserManagerPDO.php';
require_once 'modl/DBFactory.php';
$db = DBFactory::getPDOCon();
$manager = new UserManagerPDO($db);

if (isset($_SESSION['pseudo']))
{
	header('Location: Accueil.php');
}
elseif (isset($_COOKIE['username']) && isset($_COOKIE['_pwd']))
{
	$user = $manager->login(htmlspecialchars($_COOKIE['username']), htmlspecialchars($_COOKIE['_pwd']), 'cookie');
	if ($user)
	{
		session_start();
		$_SESSION['pseudo'] = $user->getPseudo();
		$_SESSION['nom'] = $user->getNom();
		$_SESSION['id'] = $user->getId();
		$_SESSION['edit_rights'] = $user->getEdit_rights();
		header('Location: Accueil.php');
	}
	else
	{
		session_start();
		$_SESSION['error_log'] = 1;
		include 'view/login/login.php';
		unset($_SESSION['error_log']);
	}
}
elseif (isset($_POST['connexion']))
{
	$user = $manager->login(htmlspecialchars($_POST['inputPseudo']), htmlspecialchars($_POST['inputPassword']), 'classic');
	if ($user)
	{
		session_start();
		$_SESSION['pseudo'] = $user->getPseudo();
		$_SESSION['nom'] = $user->getNom();
		$_SESSION['id'] = $user->getId();
		$_SESSION['edit_rights'] = $user->getEdit_rights();
		if ($_POST['remember-me'])
		{
			setcookie('username', $_SESSION['pseudo'], time() + 3600*24*365*10, null, null, false, true);
			setcookie('_pwd', $user->getPassword(), time() + 3600*24*365*10, null, null, false, true);
		}
		header('Location: Accueil.php');
	}
	else
	{
		session_start();
		$_SESSION['error_log'] = 1;
		include 'view/login/login.php';
		unset($_SESSION['error_log']);
	}
}
else
{
	include 'view/login/login.php';
}