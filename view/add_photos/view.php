<form method="post" action="view/add_photos/upload.php" enctype="multipart/form-data">
  <div class="form-group">
    <label for="photos_date">Date de la photo (jj/mm/aaaa) 
<?php 
if (isset($_SESSION['err_date']) && $_SESSION['err_date'] == 1)
{
	echo '<small><font color="red">Veuillez sélectionner une date !</font></small>';
	unset($_SESSION['err_date']);
}
?>
    </label>
    <input type="date" class="form-control" id="photos_date" placeholder="jj-mm-aaaa" name="real_date">
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" id="description" placeholder="Tapez une description" rows="4" name="description"><?php 
if (isset($_SESSION['description']))
{
	echo $_SESSION['description'];
	unset($_SESSION['description']);
}
?></textarea>
  </div>
  <div class="form-group">
    <label for="photo_file">Choisir la photo (jpg, png, max. 2Mo) 
<?php 
if (isset($_SESSION['no_file']) && $_SESSION['no_file'] == 1)
{
	echo '<small><font color="red">Veuillez sélectionner une photo.</font></small><br />';
	unset($_SESSION['no_file']);
}
?>
    </label>
    <input type="file" id="photo_file" name="photo_file">
  </div>
  <div class="checkbox">
  </div>
  <button type="submit" class="btn btn-primary" name="sending">Envoyer</button>
</form>