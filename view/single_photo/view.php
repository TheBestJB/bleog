<?php
if (isset($_SESSION['supprDesc_Success']))
{
	$wellDone = 'La description a été supprimée avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['supprDesc_Success']);
}
if (isset($_SESSION['modifDesc_Success']))
{
	$wellDone = 'La description a été modifiée avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifDesc_Success']);
}
if (isset($_SESSION['modifPhoto_Success']))
{
	$wellDone = 'La photo a été modifiée avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifPhoto_Success']);
}
if (isset($_SESSION['error_date']))
{
	$notDone = 'Le format de la date est incorrect !';
	include 'view/error_alert.php';
	unset($_SESSION['error_date']);
}
if (isset($_SESSION['modifDate_Success']))
{
	$wellDone = 'La date a été modifiée avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifDate_Success']);
}
if (isset($_SESSION['error_photo']))
{
	$notDone = 'Une erreur s\'est produite. Veuillez vérifier que le format est bien jp(e)g ou png et que le ficher n\'excède pas 2Mo !';
	include 'view/error_alert.php';
	unset($_SESSION['error_photo']);
}
if (isset($_SESSION['modifPhoto_Success']))
{
	$wellDone = 'La photo a été modifiée avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifPhoto_Success']);
}
?>
<h2 style="text-align: center">Photo du <?= $this->form_date; ?></h2>
<hr>
<?php
if (isset($_SESSION['edit_rights']) && $_SESSION['edit_rights'] == 1)
{
?>
<div class="btn-group" style="margin-bottom: 20px;">
  <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="glyphicon glyphicon-edit"></span> Modifier <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="#" data-toggle="modal" data-target="#dateEdit"><span class="glyphicon glyphicon-calendar"></span> Modifier la date</a></li>
    <li><a href="#" data-toggle="modal" data-target="#descriptionEdit"><span class="glyphicon glyphicon-align-left"></span> Modifier la description</a></li>
    <li><a href="#" data-toggle="modal" data-target="#photoEdit"><span class="glyphicon glyphicon-picture"></span> Modifier la photo</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#removeAll"><span class="glyphicon glyphicon-trash"></span> Supprimer la page</a></li>
  </ul>
</div>

<!-- Modals -->
<div class="modal fade" id="dateEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modification de la date</h4>
      </div>
      <form name="editDate" class="form-inline" action="view/single_photo/edit_photos.php?id=<?= $_GET['photoId']; ?>" method="post">
      <div class="modal-body">
        La date actuelle est : <?= $this->form_date; ?>
        <br /><br />
        	<div class="form-group">
        		<label for="photos_date">Modifier :</label> <input type="date" class="form-control" id="photos_date" placeholder="jj-mm-aaaa" name="real_date">
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" name="sendEditDate" class="btn btn-primary">Sauvegarder</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="descriptionEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modification de la description</h4>
      </div>
      <form name="editDescription" action="view/single_photo/edit_photos.php?id=<?= $_GET['photoId']; ?>" method="post">
      <div class="modal-body">
      	<div class="form-group">
        	<label for="description">Description :</label><textarea class="form-control" id="description" rows="4" name="description"><?= $this->description; ?></textarea>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
<?php 
if (!empty($this->description))
{
	echo '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#supprDesc">Supprimer la description</button>';
}
?>
        <button type="submit" name="sendEditDescription" class="btn btn-primary">Sauvegarder</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="photoEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modification de la photo</h4>
      </div>
      <form name="editPhoto" class="form-inline" action="view/single_photo/edit_photos.php?id=<?= $_GET['photoId']; ?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        Photo actuelle : <img class="img-responsive" src="pics/mini/<?= $this->picsname; ?>">
        <br /><br />
        	<div class="form-group">
        		<label for="photo_file">Choisir la photo : </label><input type="file" id="photo_file" name="photo_file">
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" name="sendEditPhoto" class="btn btn-primary">Sauvegarder</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="removeAll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Supprimer la page</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" role="alert"><b>Attention !</b> Êtes-vous sûr de vouloir supprimer définitivement cette page ?</div>
      </div>
      <form name="supprPage" action="view/single_photo/edit_photos.php?id=<?= $_GET['photoId']; ?>" method="post">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-danger" name="sendSuppr">Confirmer la suppression</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="supprDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Suppression de la description</h4>
      </div>
      <form name="supprDesc" action="view/single_photo/edit_photos.php?id=<?= $_GET['photoId']; ?>" method="post">
      <div class="modal-body">
        <div class="alert alert-danger" role="alert"><b>Attention !</b> Êtes-vous sûr de vouloir supprimer définitivement cette description ?</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" name="supprDesc" class="btn btn-danger">Supprimer la description</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- End modals -->
<?php
}
?>
<?php 
if (!empty($this->description))
{
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Description</h3>
  </div>
  <div class="panel-body">
    <?= $this->description; ?>
  </div>
</div>
<?php 
}
?>
<a href="pics/<?= $this->picsname; ?>"><img class="img-responsive" style="margin: auto;" src="pics/<?= $this->picsname; ?>"></a>