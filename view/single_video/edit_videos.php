<?php
session_start();
if ($_SESSION['edit_rights'] == 1 && isset($_GET['id']))
{
	include '../../contr/edit_videos/Edit_videos.php';
	$id = (int) $_GET['id'];
	new Edit_videos($id, $_POST);
}
else
{
	header('Location: ../../index.php');
}