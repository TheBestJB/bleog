<?php
class Monthly2
{
	protected $id;
	protected $page;
	
	public function __construct($id, $page)
	{
		$this->setId($id);
		$this->setPage($page);
		$this->reqMonth();
	}
	
	public function reqMonth()
	{
		include 'modl/monthly/Month2.php';
		new Month2($this->id, $this->page);
	}
	
	public function  setId($id)
	{
		$this->id = (int) $id;
	}
	public function getPage() {
		return $this->page;
	}
	public function setPage($page) {
		$this->page = (int) $page;
		return $this;
	}
	
}