<?php
require_once '../../modl/DBFactory.php';
/**
 * class edit_articles_DB
 */
class edit_articles_DB
{
	/**
	 * Instance PDO
	 */
	protected $db;
	
	/**
	 * Connexion à la BDD
	 * 
	 * Constructeur
	 */
	public function __construct()
	{
		$db = DBFactory::getPDOCon();
		$this->db = $db;
	}
	
	/**
	 * Modifie le titre
	 * 
	 * @param int $id
	 * @param string $title
	 */
	public function modifTitle($id, $title)
	{
		$req = $this->db->prepare('UPDATE mygen_articles SET title = :title WHERE id = :id');
		$req->execute(array(':title' => $title, ':id' => $id));
	}
	
	/**
	 * Modifie le corps de l'article
	 *
	 * @param int $id
	 * @param string $html
	 */
	public function modifArticle($id, $html)
	{
		$req = $this->db->prepare('UPDATE mygen_articles SET bodytext = :bodytext WHERE id = :id');
		$req->execute(array(':bodytext' => $html, ':id' => $id));
	}
	
	/**
	 * Modifie la date en bdd
	 * 
	 * @param int $id
	 * @param date $date
	 */
	public function modifDate($id, $date)
	{
		$req = $this->db->prepare('UPDATE mygen_articles SET reg_date = :reg_date WHERE id = :id');
		$req->execute(array(':reg_date' => $date, ':id' => $id));
	}
	
	/**
	 * Supprime l'entrée N°id
	 * 
	 * @param int $id
	 */
	public function supprPage($id)
	{
		$req = $this->db->prepare('DELETE FROM mygen_articles WHERE id = :id');
		$req->execute(array(':id' => $id));
	}
}