<?php
include '../../lib/Blog_treatments.php';
/**
 * 
 * Classe de contrôle des vidéos
 *
 */
class Add_videos extends Blog_treatments
{
	/**
	 * @var date $date
	 */
	protected $date;
	
	/**
	 * @var string $title
	 */
	protected $title;
	
	/**
	 * @var string $url
	 */
	protected $url;
	
	/**
	 * Constructeur : Validation des éléments $_POST
	 * 
	 * @param array $post
	 */
	public function __construct($post)
	{
		$vdate = $this->valid_date($post['videos_real_date']);
		$vurl = $this->valid_url($post['videos_url']);
		if (!$vdate)
		{
			$_SESSION['video_error_date'] = true;
			$_SESSION['vid_title'] = (!empty($post['videos_title'])) ? htmlspecialchars($post['videos_title']) : false;
			$_SESSION['vid_url'] = (!empty($post['videos_url'])) ? htmlspecialchars($post['videos_url']) : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		elseif (empty($post['videos_title']))
		{
			$vid_url = htmlspecialchars($post['videos_url']);
			$_SESSION['video_error_title'] = true;
			$_SESSION['vid_url'] = (!empty($vid_url)) ? $vid_url : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		elseif (!$vurl)
		{
			$url_link = htmlspecialchars($post['videos_url']);
			$_SESSION['error_link'] = true;
			$_SESSION['bad_link'] = $url_link;
			$_SESSION['vid_title'] = (!empty($post['videos_title'])) ? htmlspecialchars($post['videos_title']) : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		elseif (empty($post['videos_url']))
		{
			$_SESSION['video_error_url'] = true;
			$_SESSION['vid_title'] = (!empty($post['videos_title'])) ? htmlspecialchars($post['videos_title']) : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		else
		{
			$this->setDate($post['videos_real_date']);
			$this->setTitle($post['videos_title']);
			$this->setUrl($post['videos_url']);
			$date = $this->getDate();
			$title = $this->getTitle();
			$vid_url = $this->getUrl();
			include '../../modl/add_videos/add_videos_DB.php';
			$insert_video = new add_videos_DB($date, $title, $vid_url);
			if ($insert_video)
			{
				$_SESSION['video_success'] = true;
				header('Location: '.$_SERVER['HTTP_REFERER']);
			}
			else
			{
				$_SESSION['video_error_db'] = true;
				header('Location: '.$_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	/**
	 * Définit la date
	 * 
	 * @param date $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}
	
	/**
	 * Contrôle et définit le titre
	 * 
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$title = htmlspecialchars($title);
		$this->title = $title;
	}
	
	/**
	 * Contrôle le format du lien youtube
	 * 
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$url = htmlspecialchars($url);
		$url = str_replace('https://youtu.be/', '', $url);
		$url = str_replace('https://www.youtube.com/watch?v=', '', $url);
		$this->url = $url;
	}
	
	/**
	 * Retourne la date
	 * 
	 * @return date $date
	 */
	public function getDate()
	{
		return $this->date;
	}
	
	/**
	 * Retourne le titre
	 * 
	 * @return string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * Retourne l'url
	 * 
	 * @return string $url
	 */
	public function getUrl()
	{
		return $this->url;
	}
}