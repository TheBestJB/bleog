<?php
require_once 'modl/DBFactory.php';
require_once 'modl/UserManagerPDO.php';
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="Bootstrap/docs/favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	<link href="css/navbar-static-top.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="Bootstrap/docs/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="Bootstrap/docs/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="ckeditor/ckeditor.js"></script>
    <title>Bleog - Ajouter un article</title>
  </head>

  <body>
<?php 
new Module_nav();
echo '<br />';

?>
	<div class="container">
	<div class="jumbotron">
<?php
if (isset($_SESSION['article_success']))
{
	$wellDone = 'L\'article a été publié avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['article_success']);
}
if (isset($_SESSION['article_error_db']))
{
	$notDone = 'Une erreur s\'est produite !';
	include 'view/error_alert.php';
	unset($_SESSION['article_error_db']);
}
?>
		<h1 style="text-align: center">Ajouter un article</h1>
  		<hr>
<?php
include 'view/add_articles/view.php';
?>
     </div>
     </div>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="Bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="Bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>