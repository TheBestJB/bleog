<?php
session_start();
if ($_SESSION['edit_rights'] != 1)
{
	echo 'Error !';
}
else
{
	if (isset($_POST['suppPics']) && isset($_POST['selectedPics']))
	{
		include '../contr/add_articles/Sel_pictures.php';
		$suppPics = Sel_pictures::suppPics($_POST['selectedPics']);
	}
	else
	{
		echo 'Une erreur s\'est produite. Veuillez cocher une photo puis cliquez sur supprimer.';
	}
}