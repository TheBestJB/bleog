<?php
session_start();
session_unset();
session_destroy();
setcookie('username', '', time() - 10);
setcookie('_pwd', '', time() - 10);
header('Location: index.php');