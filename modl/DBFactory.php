<?php
class DBFactory
{
	public static function getPDOCon()
	{
		$db = new PDO('mysql:host=localhost;dbname=126609_mygenbdd', 'root', '');
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		return $db;
	}
}