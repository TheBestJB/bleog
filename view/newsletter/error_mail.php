<div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong>Erreur !</strong> L'email : <u><?= $this->email; ?></u> est déjà enregistré. Vous n'avez pas besoin de l'enregistrer à nouveau.
</div>