<?php
class PhotosView
{
	protected $id;
	protected $id_member;
	protected $picsname;
	protected $reg_date;
	protected $description;
	
	public function __construct($req = [])
	{
		if (!empty($req))
		{
			foreach ($req as $key => $value)
			{
				$method = 'set'.ucfirst($key);
				$this->$method($value);
			}
			$this->setView();
		}
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function setId_member($id_member)
	{
		$this->id_member = $id_member;
	}
	
	public function setPicsname($picsname)
	{
		$this->picsname = $picsname;
	}
	
	public function setReg_date($reg_date)
	{
		$this->reg_date = $reg_date;
	}
	
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	public function setView()
	{
		include 'view/monthly/photos/view.php';
	}
}