<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
<?php
if ($_SESSION['edit_rights'] != 1)
{
	echo 'Accès refusé !';
}
else
{
?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../Bootstrap/docs/favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	<link href="../../css/navbar-static-top.css" rel="stylesheet">
	<link href="../../css/style.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="Bootstrap/docs/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../Bootstrap/docs/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>Bleog - Liste des inscrits</title>
</head>
<body>
<?php
	require_once '../../contr/newsletter/Newsletter.php';
	$listMails = new Newsletter();
	$list = $listMails->getAllSubscribers();
?>
	<div class="container">
	<div class="jumbotron clearfix">
		<h1 style="text-align: center;">Liste des inscrits</h1>
		<hr>
		<div class="col-sm-4">
		<div class="list-group">
<?php
foreach ($list as $subscriber)
{
	echo ' <a href="#" class="list-group-item">'.$subscriber['email_address'].'</a>';
}
?>
		</div>
		</div>
	</div>
	</div>
<?php
}
?>
</body>
</html>