<?php
class ArticlesView
{
	protected $id;
	protected $id_member;
	protected $id_author;
	protected $bodytext;
	protected $reg_date;
	protected $title;
	
	public function __construct($req = [])
	{
		if (!empty($req))
		{
			foreach ($req as $key => $value)
			{
				$method = 'set'.ucfirst($key);
				$this->$method($value);
			}
			$this->setView();
		}
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function setId_member($id_member)
	{
		$this->id_member = $id_member;
	}
	
	public function setId_author($id_author)
	{
		$this->id_author = $id_author;
	}
	
	public function setBodytext($bodytext)
	{
		$this->bodytext = $bodytext;
	}
	
	public function setReg_date($reg_date)
	{
		$this->reg_date = $reg_date;
	}
	
	public function setTitle($title)
	{
		if (strlen($title) > 24)
		{
			$title = substr($title, 0, 24).'...';
			$this->title = $title;
		}
		else
		{
			$this->title = $title;
		}
	}
	
	public function setView()
	{
		include 'view/monthly/articles/view.php';
	}
}