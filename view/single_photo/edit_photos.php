<?php
session_start();
if ($_SESSION['edit_rights'] == 1 && isset($_GET['id']))
{
	include '../../contr/edit_photos/Edit_photos.php';
	$id = (int) $_GET['id'];
	new Edit_photos($id, $_POST, $_FILES);
}
else
{
	header('Location: ../../index.php');
}