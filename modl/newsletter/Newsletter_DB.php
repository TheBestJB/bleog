<?php
require_once '../../modl/DBFactory.php';
require_once '../../modl/UserManagerPDO.php';

class Newsletter_DB
{
	/**
	 * Connexion PDO
	 * 
	 * @var PDO $db
	 */
	protected $db;
	
	/**
	 * Connexion à la db puis set $this->db
	 */
	public function rcon()
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$conx = $manager->getDB();
		$this->db = $conx;
	}
	
	/**
	 * Vérifie que $email est en db
	 * @param string $email
	 * @return bool
	 */
	public function isInDb($email)
	{
		$this->rcon();
		$db = $this->db;
		$req = $db->prepare('SELECT id FROM mygen_newsletter WHERE email_address = :email');
		$req->execute(array(':email' => $email));
		$result = $req->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	/**
	 * Insert l'email en db
	 * 
	 * @param string $email
	 */
	public function putInDb($email)
	{
		$uniqid = 'unsus'.md5(rand());
		$this->rcon();
		$db = $this->db;
		$req = $db->prepare('INSERT INTO mygen_newsletter(email_address, uniqid, reg_date) VALUES(:email, :uniqid, NOW())');
		$req->execute(array(':email' => $email, ':uniqid' => $uniqid));
	}
	
	/**
	 * Sélectionne l'uniqid en fonction du mail entré en paramètre
	 * 
	 * @param string $email
	 * @return $uniqid['uniqid']
	 */
	public function getUniqid($email)
	{
		$this->rcon();
		$db = $this->db;
		$req = $db->prepare('SELECT uniqid FROM mygen_newsletter WHERE email_address = :email');
		$req->execute(array(':email' => $email));
		$uniqid = $req->fetch(PDO::FETCH_ASSOC);
		return $uniqid['uniqid'];
	}
	
	/**
	 * Supprime le mail de la db si l'id est trouvé
	 * 
	 * @param string $id
	 */
	public function deleteMail($id)
	{
		$this->rcon();
		$db =$this->db;
		$selMail = $db->prepare('SELECT email_address FROM mygen_newsletter WHERE uniqid = :id');
		$selMail->execute(array(':id' => $id));
		$mail = $selMail->fetch(PDO::FETCH_ASSOC);
		if (!empty($mail))
		{
			$req = $db->prepare('DELETE FROM mygen_newsletter WHERE uniqid = :id');
			$req->execute(array(':id' => $id));
			return $mail['email_address'];
		}
		else
		{
			return false;
		}
		
	}
	
	/**
	 * Récupère et retourne tous les mails des insrits à la newsletter
	 * 
	 * @return array $mails
	 */
	public function getAllSubscribers()
	{
		$this->rcon();
		$db = $this->db;
		$req = $db->prepare('SELECT email_address, uniqid FROM mygen_newsletter');
		$req->execute();
		$mails = $req->fetchAll(PDO::FETCH_ASSOC);
		return $mails;
	}
}