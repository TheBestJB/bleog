<?php
require_once '../../lib/Blog_treatments.php';
require_once '../../modl/edit_videos/edit_videos_DB.php';
/**
 * class Edit_videos
 */
class Edit_videos extends Blog_treatments
{
	/**
	 * @var int $id
	 */
	protected $id;
	
	/**
	 * Constructeur
	 * 
	 * @param int $id
	 * @param array $post
	 */
	public function __construct($id, $post = array())
	{
		$this->setId($id);
		if ($this->id > 0)
		{
			// Validation et modification de la date
			if (isset($post['sendEditDate']))
			{
				$vdate = $this->valid_date($post['real_date']);
				if ($vdate)
				{
					$this->modifDate($this->id, $post['real_date']);
				}
				else
				{
					$_SESSION['error_Vdate'] = true;
					header('Location: '.$_SERVER['HTTP_REFERER']);
				}
			}
			// Modification du titre
			elseif (isset($post['sendEditTitle']))
			{
				if (!empty($post['title']))
				{
					$this->modifTitle($this->id, $post['title']);
				}
				else
				{
					header('Location: '.$_SERVER['HTTP_REFERER']);
				}
			}
			// Modification de l'url
			elseif (isset($post['sendEditUrl']))
			{
				$vurl = $this->valid_url($post['url_link']);
				if ($vurl)
				{
					$this->modifUrl($this->id, $post['url_link']);
				}
				else
				{
					$_SESSION['error_Vurl'] = true;
					header('Location: '.$_SERVER['HTTP_REFERER']);
				}
			}
			// Suppression de la page
			elseif (isset($post['sendSuppr']))
			{
				$this->supprPage($this->id);
			}
			else
			{
				header('Location: ../../index.php');
			}
		}
		else
		{
			header('Location: ../../index.php');
		}
	}
	
	/**
	 * Setter de l'attribut $id
	 * 
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = (int) $id;
	}
	
	/**
	 * Envoie le nouveau titre au modèle
	 * 
	 * @param int $id
	 * @param string $title
	 */
	public function modifTitle($id, $title)
	{
		$obj = new edit_videos_DB();
		$title = htmlspecialchars($title);
		$obj->modifTitle($id, $title);
		$_SESSION['modifVTitle_Success'] = true;
		header('Location: ../../videos.php?videoId='.$this->id);
	}
	
	/**
	 * Envoie le nouveau lien au modèle
	 *
	 * @param int $id
	 * @param string $html
	 */
	public function modifUrl($id, $url)
	{
		$obj = new edit_videos_DB();
		$html = htmlspecialchars($url);
		$url = str_replace('https://youtu.be/', '', $url);
		$url = str_replace('https://www.youtube.com/watch?v=', '', $url);
		$obj->modifUrl($id, $url);
		$_SESSION['modifUrl_Success'] = true;
		header('Location: ../../videos.php?videoId='.$this->id);
	}
	
	/**
	 * Envoie nouvelle date au modèle
	 * 
	 * @param int $id
	 * @param date $date
	 */
	public function modifDate($id, $date)
	{
		$obj = new edit_videos_DB();
		$obj->modifDate($id, $date);
		$_SESSION['modifVDate_Success'] = true;
		header('Location: '.$_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Demande au modèle de supprimer la page
	 * 
	 * @param int $id
	 */
	public function supprPage($id)
	{
		$obj = new edit_videos_DB();
		$obj->supprPage($id);
		$_SESSION['supprPage_Success'] = true;
		header('Location: ../../index.php');
	}
}