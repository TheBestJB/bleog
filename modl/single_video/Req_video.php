<?php
/**
 * 
 * Classe de requête de vidéo
 *
 */
class Req_video
{
	/**
	 * @var id video
	 */
	protected $id;
	
	/**
	 * @var id membre
	 */
	protected $id_member;
	
	/**
	 * @var Titre
	 */
	protected $url_title;
	
	/**
	 * @var Url
	 */
	protected $url_link;
	
	/**
	 * @var Date de publication
	 */
	protected $reg_date;
	
	/**
	 * Vérifie si l'id existe en db
	 * 
	 * @param int $id
	 */
	public function verifId($id)
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$conx = $manager->getDB();
		$req = $conx->prepare('SELECT id FROM mygen_videos WHERE id = :id');
		$req->execute(array(':id' => $id));
		$result = $req->fetch(PDO::FETCH_ASSOC);
		$valid = $result ? true : false;
		if ($valid)
		{
			$selVid = $conx->prepare('SELECT id, id_member, url_title, url_link, reg_date FROM mygen_videos WHERE id = :id');
			$selVid->execute(array(':id' => $id));
			$data = $selVid->fetch(PDO::FETCH_ASSOC);
			foreach ($data as $key => $value)
			{
				$method = 'set'.ucfirst($key);
				$this->$method($value);
			}
		}
		return $valid;
	}
	
	/**
	 * Setter $this->id
	 * 
	 * @param $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	 * Setter $this->id_member
	 * 
	 * @param $id_member
	 */
	public function setId_member($id_member)
	{
		$this->id_member = $id_member;
	}
	
	/**
	 * Setter $this->title
	 * 
	 * @param $title
	 */
	public function setUrl_title($title)
	{
		$this->url_title = $title;
	}
	
	/**
	 * Setter $this->url
	 * 
	 * @param $url
	 */
	public function setUrl_link($url)
	{
		$this->url_link = $url;
	}
	
	/**
	 * Setter $this->reg_date
	 * 
	 * @param $reg_date
	 */
	public function setReg_date($reg_date)
	{
		$date = date('d/m/Y', strtotime($reg_date));
		$this->reg_date = $date;
	}
	
	// GETTERS
	
	/**
	 * @return $this->id
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @return $this->id_member
	 */
	public function getId_member()
	{
		return $this->id_member;
	}
	
	/**
	 * @return $this->title
	 */
	public function getUrl_title()
	{
		return $this->url_title;
	}
	
	/**
	 * @return $this->url_link
	 */
	public function getUrl_link()
	{
		return $this->url_link;
	}
	
	/**
	 * @return $this->reg_date
	 */
	public function getReg_date()
	{
		return $this->reg_date;
	}
}