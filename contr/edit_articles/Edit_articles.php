<?php
require_once '../../lib/Blog_treatments.php';
require_once '../../modl/edit_articles/edit_articles_DB.php';
/**
 * class Edit_articles
 */
class Edit_articles extends Blog_treatments
{
	/**
	 * @var int $id
	 */
	protected $id;
	
	/**
	 * Constructeur
	 * 
	 * @param int $id
	 * @param array $post
	 */
	public function __construct($id, $post = array())
	{
		$this->setId($id);
		if ($this->id > 0)
		{
			// Validation et modification de la date
			if (isset($post['sendEditDate']))
			{
				$vdate = $this->valid_date($post['real_date']);
				if ($vdate)
				{
					$this->modifDate($this->id, $post['real_date']);
				}
				else
				{
					$_SESSION['error_date'] = true;
					header('Location: '.$_SERVER['HTTP_REFERER']);
				}
			}
			// Modification du titre
			elseif (isset($post['sendEditTitle']))
			{
				if (!empty($post['title']))
				{
					$this->modifTitle($this->id, $post['title']);
				}
				else
				{
					header('Location: '.$_SERVER['HTTP_REFERER']);
				}
			}
			// Modification de l'article
			elseif (isset($post['article_sending']))
			{
				if (!empty($post['article_description']))
				{
					$this->modifArticle($this->id, $post['article_description']);
				}
				else
				{
					header('Location: ../../articles.php?articleId='.$this->id);
				}
			}
			// Annulation de l'éditon de l'article
			elseif (isset($post['cancelBody']))
			{
				header('Location: ../../articles.php?articleId='.$this->id);
			}
			// Suppression de la page
			elseif (isset($post['sendSuppr']))
			{
				$this->supprPage($this->id);
			}
			else
			{
				header('Location: ../../index.php');
			}
		}
		else
		{
			header('Location: ../../index.php');
		}
	}
	
	/**
	 * Setter de l'attribut $id
	 * 
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = (int) $id;
	}
	
	/**
	 * Envoie le nouveau titre au modèle
	 * 
	 * @param int $id
	 * @param string $title
	 */
	public function modifTitle($id, $title)
	{
		$obj = new edit_articles_DB();
		$title = htmlspecialchars($title);
		$obj->modifTitle($id, $title);
		$_SESSION['modifTitle_Success'] = true;
		header('Location: ../../articles.php?articleId='.$this->id);
	}
	
	/**
	 * Envoie le nouveau texte de l'article au modèle
	 *
	 * @param int $id
	 * @param string $html
	 */
	public function modifArticle($id, $html)
	{
		$obj = new edit_articles_DB();
		$html = htmlspecialchars($html);
		$obj->modifArticle($id, $html);
		$_SESSION['modifArticle_Success'] = true;
		header('Location: ../../articles.php?articleId='.$this->id);
	}
	
	/**
	 * Envoie nouvelle date au modèle
	 * 
	 * @param int $id
	 * @param date $date
	 */
	public function modifDate($id, $date)
	{
		$obj = new edit_articles_DB();
		$obj->modifDate($id, $date);
		$_SESSION['modifDate_Success'] = true;
		header('Location: '.$_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Demande au modèle de supprimer la page
	 * 
	 * @param int $id
	 */
	public function supprPage($id)
	{
		$obj = new edit_articles_DB();
		$obj->supprPage($id);
		$_SESSION['supprPage_Success'] = true;
		header('Location: ../../index.php');
	}
}