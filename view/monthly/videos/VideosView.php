<?php
class VideosView
{
	protected $id;
	protected $id_member;
	protected $url_link;
	protected $reg_date;
	protected $url_title;
	
	public function __construct($req = [])
	{
		if (!empty($req))
		{
			foreach ($req as $key => $value)
			{
				$method = 'set'.ucfirst($key);
				$this->$method($value);
			}
			$this->setView();
		}
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function setId_member($id_member)
	{
		$this->id_member = $id_member;
	}
	
	public function setUrl_link($url_link)
	{
		$this->url_link = $url_link;
	}
	
	public function setReg_date($reg_date)
	{
		$this->reg_date = $reg_date;
	}
	
	public function setUrl_title($url_title)
	{
		if (strlen($url_title) > 24)
		{
			$url_title = substr($url_title, 0, 24).'...';
			$this->url_title = $url_title;
		}
		else
		{
			$this->url_title = $url_title;
		}
	}
	
	public function setView()
	{
		include 'view/monthly/videos/view.php';
	}
}