<?php
require_once 'modl/DBFactory.php';
require_once 'modl/UserManagerPDO.php';
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="Bootstrap/docs/favicon.ico">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	<link href="css/navbar-static-top.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
    <script src="Bootstrap/docs/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>Bleog - Accueil</title>
  </head>

  <body>
<?php 
new Module_nav();
//echo '<br />';

?>
	<div class="container">
		<img class="img-responsive" src="css/eog.jpg" style="margin-top: -19px;margin-bottom: -50px">
	<div class="jumbotron"">
<?php
if (isset($_SESSION['supprPage_Success']))
{
	$wellDone = 'La page a été supprimée avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['supprPage_Success']);
}
?>
		<!-- <img alt="baby" class="img-responsive" src="css/baby.gif" style="width: 8%; position: absolute;"> -->
		<h1 style="text-align: center">Blog d'Eogan</h1>
  		<hr>
			<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 0px" id="emldiv">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<form class="form-horizontal" method="post" action="" id="sub">
				<div class="form-group" style="margin-bottom: 0px;">
					<label class="control-label col-sm-4" for="inputGroupSuccess2">Recevoir un mail à chaque nouveauté</label>
					<div class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon">@</span>
							<input type="email" class="form-control" id="mail" name="name" aria-describedby="inputGroupSuccess2Status">
						</div>
					</div>
					<button type="submit" class="btn btn-default" id="submit" name="send">Confirmer</button>
				</div>
				</form>
			</div>
<?php
if (!isset($_GET['p']) || $_GET['p'] <= 0)
	$_GET['p'] = 1;
$month = new Monthly('1', $_GET['p']);
?>
     </div>
     </div>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="Bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="Bootstrap/js/bootstrap.min.js"></script>
    <script src="js/newsletter.js"></script>
  </body>
</html>