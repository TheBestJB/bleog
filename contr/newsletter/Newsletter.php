<?php
include '../../lib/Blog_treatments.php';
include '../../modl/newsletter/Newsletter_DB.php';

class Newsletter extends Blog_treatments
{
	/**
	 * Contient l'email (à valider) du formulaire
	 * 
	 * @var string $email
	 */
	protected $email;
	
	/**
	 * Contient l'uniqid d'un email
	 * 
	 * @var string $uniqid
	 */
	protected $uniqid;
	
	/**
	 * Vérifie la validité du mail puis intérroge le modèle
	 * puis appelle la vue correspondante.
	 * 
	 * @param email $email
	 */
	public function verifMail($email)
	{
		$email = htmlspecialchars($email);
		$this->email = $email;
		$vmail = $this->valid_newsletter($this->email);
		//Si l'email est valide
		if ($vmail)
		{
			$inDb = new Newsletter_DB();
			$res = $inDb->isInDb($this->email);
			//Si l'adresse est déjà en DB
			if ($res)
			{
				include_once '../../view/newsletter/error_mail.php';
			}
			else 
			{
				$inDb->putInDb($this->email);
				include_once '../../view/newsletter/success_mail.php';
				$this->sendMail($this->email);
				$this->sendNewSubscriber();
			}
		}
		else
		{
			include_once '../../view/newsletter/invalid_mail.php';
		}
	}
	
	/**
	 * Envoi de mail aux admins pour notifier
	 * une nouvelle inscription.
	 * 
	 * @param string $email
	 */
	public function sendMail($email)
	{
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$message = 'Un nouvel abonné vient de s\'inscrire à la newsletter.<br>
				Voici son adresse : <strong>'.$email.'</strong><br><br>
						<a href="http://www.bleog.fr"><strong>Bleog.fr</strong></a>';
		mail('thebestjb@gmail.com, djenyka@gmail.com', 'Nouvel abonné sur Bleog', $message, $headers);
	}
	
	/**
	 * Envoi un email de confirmation à un nouvel inscrit
	 */
	public function sendNewSubscriber()
	{
		$getId = $this->setUniqid($this->email);
		$uniqid = $this->uniqid;
		$to = $this->email;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$message = '<h1>Bleog.fr</h1><br><br>
				Merci de vous être inscrit à la newsletter <a href="http://www.bleog.fr"><strong>Bleog.fr</strong></a><br><br>
				Vous recevrez un email dès qu\'une nouvelle photo, vidéo ou qu\'un nouvel article paraît sur le site.<br><br>
				<hr>
				<small>Pour ne plus recevoir les newsletters, vous pouvez vous désinscrire en cliquant <a href="http://www.bleog.fr/unsubscribe_newsletter.php?id='.$uniqid.'">ici</a></small>';
		mail($to, 'Newsletter Bleog.fr', $message, $headers);
	}
	
	/**
	 * Setter de l'uniqid
	 * 
	 * @param string $email
	 */
	public function setUniqid($email)
	{
		$theId = new Newsletter_DB();
		$uniqid = $theId->getUniqid($email);
		$this->uniqid = $uniqid;
		
	}
	
	public function deleteMail($id)
	{
		$obj = new Newsletter_DB();
		$del = $obj->deleteMail($id);
		if (!$del)
		{
			return 'Adresse email non trouvée !<br><br>Retourner sur <a href="http://www.bleog.fr">Bleog.fr</a>';
		}
		else
		{
			return 'L\'adresse email <u>'.$del.'</u> a bien été supprimée et ne recevra plus de newsletters.
					<br><br>Retourner sur <a href="http://www.bleog.fr">Bleog.fr</a>';
		}
	}
	
	/**
	 * Méthode d'envoi de la newsletter à tous les inscrits
	 * 
	 * @param array $post
	 */
	public function sendToAllSubscribers($post)
	{
		if (empty($post['newsletters_title']))
		{
			$_SESSION['newsletter_error_title'] = true;
			$_SESSION['news_desc'] = (!empty($post['newsletters_message'])) ? $post['newsletters_message'] : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		elseif (empty($post['newsletters_message']))
		{
			$_SESSION['newsletter_error_desc'] = true;
			$_SESSION['news_title'] = (!empty($post['newsletters_title'])) ? $post['newsletters_title'] : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		elseif (!empty($post['newsletters_title']) && !empty($post['newsletters_message']))
		{
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$mails = new Newsletter_DB();
			$tabMails = $mails->getAllSubscribers();
			foreach ($tabMails as $mail)
			{
				$to = $mail['email_address'];
				$message = $post['newsletters_message'];
				$message .= '<br><a href="http://www.bleog.fr"><strong>Bleog.fr</strong></a><hr>
						<small>Pour ne plus recevoir les newsletters, vous pouvez vous désinscrire en cliquant
						<a href="http://www.bleog.fr/unsubscribe_newsletter.php?id='.$mail['uniqid'].'">ici</a></small>';
				mail($to, $post['newsletters_title'], $message, $headers);
			}
			$_SESSION['success_newsletter'] = true;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
	}
	
	/**
	 * Retourne un tableau contenant tous les inscrits
	 * 
	 * @return array $list
	 */
	public function getAllSubscribers()
	{
		$listMails = new Newsletter_DB();
		$list = $listMails->getAllSubscribers();
		return $list;
	}
}