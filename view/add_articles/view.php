<?php 
if (isset($_SESSION['article_error_date']))
{
	$err_date = '<small><font color="red"><b> Veuillez entrer une date valide !</b></font></small>';
	unset($_SESSION['article_error_date']);
}
else
{
	$err_date = null;
}
if (isset($_SESSION['article_error_title']))
{
	$err_title = '<small><font color="red"><b> Veuillez entrer un titre !</b></font></small>';
	unset($_SESSION['article_error_title']);
}
else
{
	$err_title = null;
}
if (isset($_SESSION['article_error_desc']))
{
	$err_desc = '<small><font color="red"><b> Veuillez entrer un article !</b></font></small>';
	unset($_SESSION['article_error_desc']);
}
else
{
	$err_desc = null;
}
if (isset($_SESSION['art_title']))
{
	$art_title = 'value="'.$_SESSION['art_title'].'"';
	unset($_SESSION['art_title']);
}
else
{
	$art_title = null;
}
if (isset($_SESSION['art_desc']))
{
	$art_desc = $_SESSION['art_desc'];
	unset($_SESSION['art_desc']);
}
else
{
	$art_desc = null;
}
?>
<form method="post" action="view/add_articles/upload.php" enctype="multipart/form-data">
  <div class="form-group">
    <label for="articles_date">Date de l'article (jj/mm/aaaa)</label><?= $err_date; ?>
    <input type="date" class="form-control" id="articles_date" placeholder="jj-mm-aaaa" name="articles_real_date">
  </div>
  <div class="form-group">
    <label for="articles_title">Titre de l'article :</label><?= $err_title; ?>
    <input type="text" class="form-control" id="articles_title" placeholder="Veuillez entrer un titre" <?= $art_title; ?> name="articles_title">
  </div>
  <div class="form-group">
    <label for="articles_body">Article :</label><?= $err_desc; ?>
    <textarea class="form-control" id="articles_body" placeholder="Tapez l'article" rows="4" name="article_description"><?= $art_desc; ?></textarea>
  </div>
  <button type="submit" class="btn btn-primary" name="article_sending">Envoyer</button>
</form>
<script>CKEDITOR.replace('article_description', {
	filebrowserBrowseUrl: '/articles_pics/sel_pics.php',
    filebrowserUploadUrl: '/articles_pics/upload.php'
});</script>