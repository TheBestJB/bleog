<div class="alert alert-danger alert-dismissible" role="alert" id="emldiv">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<form class="form-horizontal" method="post" action="" id="sub">
  <div class="form-group">
    <label class="control-label col-sm-4" for="inputGroupSuccess2"><span class="glyphicon glyphicon-warning-sign"></span> Adresse invalide, réessayer :</label>
    <div class="col-sm-4">
      <div class="input-group">
        <span class="input-group-addon">@</span>
        <input type="email" class="form-control" id="mail" name="name" aria-describedby="inputGroupSuccess2Status">
      </div>
    </div>
    <button type="submit" class="btn btn-default" id="submit" name="send">Confirmer</button>
  </div>
</form>
</div>