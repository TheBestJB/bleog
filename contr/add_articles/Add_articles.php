<?php
include '../../lib/Blog_treatments.php';
/**
 * 
 * Classe de contrôle des articles
 *
 */
class Add_articles extends Blog_treatments
{
	/**
	 * @var date $date
	 */
	protected $date;
	
	/**
	 * @var string $title
	 */
	protected $title;
	
	/**
	 * @var string $description
	 */
	protected $description;
	
	/**
	 * Constructeur : Validation des éléments $_POST
	 * 
	 * @param array $post
	 */
	public function __construct($post)
	{
		$vdate = $this->valid_date($post['articles_real_date']);
		if (!$vdate)
		{
			$_SESSION['article_error_date'] = true;
			$_SESSION['art_title'] = (!empty($post['articles_title'])) ? htmlspecialchars($post['articles_title']) : false;
			$_SESSION['art_desc'] = (!empty($post['article_description'])) ? htmlspecialchars($post['article_description']) : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		elseif (empty($post['articles_title']))
		{
			$description = htmlspecialchars($post['article_description']);
			$_SESSION['article_error_title'] = true;
			$_SESSION['art_desc'] = (!empty($description)) ? $description : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		elseif (empty($post['article_description']))
		{
			$_SESSION['article_error_desc'] = true;
			$_SESSION['art_title'] = (!empty($post['articles_title'])) ? htmlspecialchars($post['articles_title']) : false;
			header('Location: '.$_SERVER['HTTP_REFERER']);
		}
		else
		{
			$this->setDate($post['articles_real_date']);
			$this->setTitle($post['articles_title']);
			$this->setDescription($post['article_description']);
			$date = $this->getDate();
			$title = $this->getTitle();
			$description = $this->getDescription();
			include '../../modl/add_articles/add_articles_DB.php';
			$insert_article = new add_aticles_DB($date, $title, $description);
			if ($insert_article)
			{
				$_SESSION['article_success'] = true;
				header('Location: '.$_SERVER['HTTP_REFERER']);
			}
			else
			{
				$_SESSION['article_error_db'] = true;
				header('Location: '.$_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	/**
	 * Définit la date
	 * 
	 * @param date $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}
	
	/**
	 * Contrôle et définit le titre
	 * 
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$title = htmlspecialchars($title);
		$this->title = $title;
	}
	
	/**
	 * Contrôle et définit la description
	 * 
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$description = htmlspecialchars($description);
		$this->description = $description;
	}
	
	/**
	 * Retourne la date
	 * 
	 * @return date $date
	 */
	public function getDate()
	{
		return $this->date;
	}
	
	/**
	 * Retourne le titre
	 * 
	 * @return string $title
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * Retourne la description
	 * 
	 * @return string $description
	 */
	public function getDescription()
	{
		return $this->description;
	}
}