<?php
session_start();
if (isset($_GET['id']) && !isset($_SESSION['returnInfoMail']))
{
	$_SESSION['delMail'] = htmlspecialchars($_GET['id']);
	$body = '<p>Veuillez cliquer sur "Valider" pour confirmer votre désinscription à la newsletter.</p>
			<br>
			<form action="view/newsletter/unsubscribe.php" method="post"><button type="submit" name="sendDel" value="valider">Valider</button></form>';
}
elseif (isset($_SESSION['returnInfoMail']))
{
	$body = $_SESSION['returnInfoMail'];
	session_unset();
	session_destroy();
}
else
{
	header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
<?php 
echo $body;
?>
</body>
</html>