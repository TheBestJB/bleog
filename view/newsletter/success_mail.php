<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong>Bravo !</strong> Vous recevrez un email à chaque nouveauté à l'adresse suivante : <u><?= $this->email; ?></u>
</div>