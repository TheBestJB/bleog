<?php
require_once '../../modl/DBFactory.php';
/**
 * class edit_videos_DB
 */
class edit_videos_DB
{
	/**
	 * Instance PDO
	 */
	protected $db;
	
	/**
	 * Connexion à la BDD
	 * 
	 * Constructeur
	 */
	public function __construct()
	{
		$db = DBFactory::getPDOCon();
		$this->db = $db;
	}
	
	/**
	 * Modifie le titre
	 * 
	 * @param int $id
	 * @param string $title
	 */
	public function modifTitle($id, $title)
	{
		$req = $this->db->prepare('UPDATE mygen_videos SET url_title = :title WHERE id = :id');
		$req->execute(array(':title' => $title, ':id' => $id));
	}
	
	/**
	 * Modifie le lien youtube
	 *
	 * @param int $id
	 * @param string $url
	 */
	public function modifUrl($id, $url)
	{
		$req = $this->db->prepare('UPDATE mygen_videos SET url_link = :url WHERE id = :id');
		$req->execute(array(':url' => $url, ':id' => $id));
	}
	
	/**
	 * Modifie la date en bdd
	 * 
	 * @param int $id
	 * @param date $date
	 */
	public function modifDate($id, $date)
	{
		$req = $this->db->prepare('UPDATE mygen_videos SET reg_date = :reg_date WHERE id = :id');
		$req->execute(array(':reg_date' => $date, ':id' => $id));
	}
	
	/**
	 * Supprime l'entrée N°id
	 * 
	 * @param int $id
	 */
	public function supprPage($id)
	{
		$req = $this->db->prepare('DELETE FROM mygen_videos WHERE id = :id');
		$req->execute(array(':id' => $id));
	}
}