<?php
session_start();
if ($_SESSION['edit_rights'] != 1)
{
	echo 'Error !';
}
else
{
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="Bootstrap/docs/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../Bootstrap/docs/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>Bleog - Sélectionner une photo</title>
  </head>

  <body>
  	<div class="row">
<?php
	include '../contr/add_articles/Sel_pictures.php';
	$pics = new Sel_pictures();
	$pics_array = $pics->getData();
	
	if (empty($pics_array))
	{
		echo '<h1 style="text-align: center;">Aucune photo n\'a été ajoutée !</h1>';
	}
	else
	{
		foreach ($pics_array as $pic)
		{
?>
		<div class="col-sm-6 col-md-2">
			<div class="thumbnail">
				<form method="post" action="suppPics.php">
				<img src="/pics/articles_pics/mini/<?= $pic['picsname']; ?>" alt="<?= $pic['picsname']; ?>">
				<div class="caption">
					<small>Lien à copier :</small>
					<pre>/pics/articles_pics/mini/<?= $pic['picsname']; ?></pre>
					<input type="checkbox" name="selectedPics" value="<?= $pic['picsname']; ?>"> <small>Cochez et cliquez sur Supprimer</small>
					<button type="submit" class="btn btn-danger btn-xs" name="suppPics">Supprimer</button>
				</form>
				</div>
			</div>
		</div>
<?php
		}
	}
?>
	</div>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../Bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="../../Bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
<?php
}
?>