<?php
session_start();
function loadClass($className)
{
	include 'contr/'.strtolower($className).'/'.$className.'.php';
}
spl_autoload_register('loadClass');
if ($_SESSION['edit_rights'] == 1)
{
	require_once 'view/add_articles/add_articles_logged.php';
}
else
{
	header('Location: index.php');
}