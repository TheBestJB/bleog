<?php
require '../modl/add_articles/sel_pictures_DB.php';

class Sel_pictures
{
	/**
	 * Contient le tableau de données (photos).
	 * 
	 * @var array
	 */
	protected $data;
	
	/**
	 * Constructeur par défaut. Demande au modèle de sélectionner tous les photos.
	 */
	public function __construct()
	{
		$call_pictures = new sel_pictures_DB();
		$data = $call_pictures->getData();
		$this->setData($data);
	}
	
	/**
	 * Demande au modèle de supprimer la photo sélectionnée.
	 * 
	 * @param array $picsname
	 */
	public static function suppPics($picsname)
	{
		$suppPics = sel_pictures_DB::suppPicsDB($picsname);
		header('Location: '.$_SERVER['HTTP_REFERER']);
		
	}
	
	/**
	 * Setter de $this->data, tableau contenant toutes les photos en db.
	 * 
	 * @param array $data
	 */
	private function setData($data)
	{
		$this->data = $data;
	}
	
	/**
	 * Getter de $this->data, retourne le tableau de données (toutes les photos en db).
	 */
	public function getData()
	{
		return $this->data;
	}
}