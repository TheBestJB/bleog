<?php
require '../modl/DBFactory.php';
require '../modl/UserManagerPDO.php';

class sel_pictures_DB
{
	/**
	 * Contient le tableau de données de la requête.
	 * 
	 * @var array $data
	 */
	protected $data = [];
	
	/**
	 * Constructeur requêtant les photos en db.
	 */
	public function __construct()
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$conx = $manager->getDB();
		$req = $conx->prepare('SELECT picsname FROM mygen_articles_pics WHERE id_member = :id_member ORDER BY reg_date DESC');
		$req->execute(array(':id_member' => 1));
		$data = $req->fetchAll(PDO::FETCH_ASSOC);;
		$this->setData($data);
	}
	
	/**
	 * Setter du tableau $this->data
	 * 
	 * @param array $data
	 */
	private function setData($data)
	{
		$this->data = $data;
	}
	
	/**
	 * Getter de $this->data
	 * 
	 * @return array $this->data
	 */
	public function getData()
	{
		return $this->data;
	}
	
	/**
	 * Supprime la photo passée en paramètre de la db.
	 * 
	 * @param string $picsname
	 */
	public static function suppPicsDB($picsname)
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$conx = $manager->getDB();
		$req = $conx->prepare('DELETE FROM mygen_articles_pics WHERE picsname = :picsname');
		$req->execute(array(':picsname' => $picsname));
	}
}