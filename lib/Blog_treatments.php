<?php
/**
 * Classe d'actions de traitements sur l'ajout/modification des pages photos
 */
abstract class Blog_treatments
{
	/**
	 * Contient le nom généré de la photo
	 * 
	 * @var random string
	 */
	protected $rand_picsname;
	
	/**
	 * Regex de validation de la date au format : yyyy-mm-dd
	 * 
	 * @param date $date
	 * @return boolean
	 */
	public function valid_date($date)
	{
		return preg_match('#^\d{4}-\d{1,2}-\d{1,2}$#', $date);
	}
	
	/**
	 * Regex de validation d'un lien youtube
	 * 
	 * @param string $url
	 * @return bool
	 */
	public function valid_url($url)
	{
		return preg_match('/^(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([\w-]{10,})/', $url);
	}
	
	/**
	 * Validation d'une photo au format jpg/jpeg ou png
	 *
	 * @param array $files
	 * @return boolean
	 */
	public function valid_photo($files)
	{
		$valid_ext = array('jpg', 'jpeg', 'png');
		$real_ext = strtolower((substr(strrchr($files['photo_file']['name'], '.'), 1)));
		if ($files['photo_file']['error'] > 0 || $files['photo_file']['size'] > 2097152 || !in_array($real_ext, $valid_ext) || !getimagesize($files['photo_file']['tmp_name']))
		{
			return false;
		}
		else
		{
			$rand_name = md5(uniqid(rand(), true));
			$file_name = $rand_name.'.'.$real_ext;
			if ($real_ext == 'jpeg' || $real_ext == 'jpg')
			{
				$src_file = imagecreatefromjpeg($files['photo_file']['tmp_name']);
				$dest_file = imagecreatetruecolor(220, 147);
				$src_width = imagesx($src_file);
				$src_height = imagesy($src_file);
				$dest_width = imagesx($dest_file);
				$dest_height = imagesy($dest_file);
				imagecopyresampled($dest_file, $src_file, 0, 0, 0, 0, $dest_width, $dest_height, $src_width, $src_height);
				imagejpeg($dest_file, '../../pics/mini/'.$rand_name.'.'.$real_ext);
				move_uploaded_file($files['photo_file']['tmp_name'], '../../pics/'.$rand_name.'.'.$real_ext);
			}
			elseif ($real_ext == 'png')
			{
				$src_file = imagecreatefrompng($files['photo_file']['tmp_name']);
				$dest_file = imagecreatetruecolor(220, 147);
				$src_width = imagesx($src_file);
				$src_height = imagesy($src_file);
				$dest_width = imagesx($dest_file);
				$dest_height = imagesy($dest_file);
				imagecopyresampled($dest_file, $src_file, 0, 0, 0, 0, $dest_width, $dest_height, $src_width, $src_height);
				imagepng($dest_file, '../../pics/mini/'.$rand_name.'.'.$real_ext);
				move_uploaded_file($files['photo_file']['tmp_name'], '../../pics/'.$rand_name.'.'.$real_ext);
			}
			$this->rand_picsname = $file_name;
			return true;
		}
	}
	
	/**
	 * Validation d'une photo au format jpg/jpeg ou png dans ajout d'article
	 *
	 * @param array $files
	 * @return boolean
	 */
	public function valid_articles_pics($files)
	{
		$valid_ext = array('jpg', 'jpeg', 'png');
		$real_ext = strtolower((substr(strrchr($files['upload']['name'], '.'), 1)));
		if ($files['upload']['error'] > 0 || $files['upload']['size'] > 2097152 || !in_array($real_ext, $valid_ext) || !getimagesize($files['upload']['tmp_name']))
		{
			return false;
		}
		else
		{
			$rand_name = md5(uniqid(rand(), true));
			$file_name = $rand_name.'.'.$real_ext;
			if ($real_ext == 'jpeg' || $real_ext == 'jpg')
			{
				$src_file = imagecreatefromjpeg($files['upload']['tmp_name']);
				$dest_file = imagecreatetruecolor(220, 147);
				$src_width = imagesx($src_file);
				$src_height = imagesy($src_file);
				$dest_width = imagesx($dest_file);
				$dest_height = imagesy($dest_file);
				imagecopyresampled($dest_file, $src_file, 0, 0, 0, 0, $dest_width, $dest_height, $src_width, $src_height);
				imagejpeg($dest_file, '../pics/articles_pics/mini/'.$rand_name.'.'.$real_ext);
				move_uploaded_file($files['upload']['tmp_name'], '../pics/articles_pics/'.$rand_name.'.'.$real_ext);
			}
			elseif ($real_ext == 'png')
			{
				$src_file = imagecreatefrompng($files['upload']['tmp_name']);
				$dest_file = imagecreatetruecolor(220, 147);
				$src_width = imagesx($src_file);
				$src_height = imagesy($src_file);
				$dest_width = imagesx($dest_file);
				$dest_height = imagesy($dest_file);
				imagecopyresampled($dest_file, $src_file, 0, 0, 0, 0, $dest_width, $dest_height, $src_width, $src_height);
				imagepng($dest_file, '../pics/articles_pics/mini/'.$rand_name.'.'.$real_ext);
				move_uploaded_file($files['upload']['tmp_name'], '../pics/articles_pics/'.$rand_name.'.'.$real_ext);
			}
			$this->rand_picsname = $file_name;
			return true;
		}
	}
	
	/**
	 * Vérifie la validité de l'email
	 * 
	 * @param string $email
	 */
	public function valid_newsletter($email)
	{
		return preg_match('#^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$#', $email);
	}
}