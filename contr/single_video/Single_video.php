<?php
/**
 * 
 * Classe de contrôle de l'id vidéo
 *
 */
class Single_video
{
	/**
	 * Constructeur
	 * 
	 * @param int $id
	 */
	public function __construct($id)
	{
		$id = (int) $id;
		if ($id > 0)
		{
			require 'modl/single_video/Req_video.php';
			$vvid = new Req_video();
			if ($vvid->verifId($id))
			{
				include 'view/single_video/view.php';
			}
			else
			{
				echo '<h2 style="text-align: center;">Désolé, cette page n\'existe pas !</h2>';
			}
		}
		else
		{
			echo '<h2 style="text-align: center;">Désolé, cette page n\'existe pas !</h2>';
		}
	}
}