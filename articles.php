<?php
session_start();

function loadClass($className)
{
	include 'contr/'.strtolower($className).'/'.$className.'.php';
}
spl_autoload_register('loadClass');

if (isset($_SESSION['pseudo']))
{
	require_once 'view/articles/articles_logged.php';
}
else
{
	header('Location: index.php');
}