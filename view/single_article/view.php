<?php
if (isset($_SESSION['modifTitle_Success']))
{
	$wellDone = 'Le titre a été modifié avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifTitle_Success']);
}
if (isset($_SESSION['error_date']))
{
	$notDone = 'Le format de la date est incorrect !';
	include 'view/error_alert.php';
	unset($_SESSION['error_date']);
}
if (isset($_SESSION['modifDate_Success']))
{
	$wellDone = 'La date a été modifiée avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifDate_Success']);
}
if (isset($_SESSION['modifArticle_Success']))
{
	$wellDone = 'L\'article a été modifié avec succès !';
	include 'view/success_alert.php';
	unset($_SESSION['modifArticle_Success']);
}
?>
<h2 style="text-align: center;">Article du <?= $vart->getReg_date(); ?></h2>
<hr>
<!-- Bouton Edit -->
<?php
if (!isset($_GET['edit']) || $_GET['edit'] != 1)
{
	if ($_SESSION['edit_rights'] == 1)
	{
?>
<div class="btn-group" style="margin-bottom: 20px;">
  <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="glyphicon glyphicon-edit"></span> Modifier <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="#" data-toggle="modal" data-target="#dateEdit"><span class="glyphicon glyphicon-calendar"></span> Modifier la date</a></li>
    <li><a href="#" data-toggle="modal" data-target="#titleEdit"><span class="glyphicon glyphicon-align-left"></span> Modifier le titre</a></li>
    <li><a href="articles.php?articleId=<?= $vart->getId(); ?>&edit=1"><span class="glyphicon glyphicon-wrench"></span> Modifier l'article</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#removeAll"><span class="glyphicon glyphicon-trash"></span> Supprimer la page</a></li>
  </ul>
</div>
<!-- Fin Bouton Edit -->
<!-- Modals -->
<div class="modal fade" id="dateEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modification de la date</h4>
      </div>
      <form name="editDate" class="form-inline" action="view/single_article/edit_articles.php?id=<?= $_GET['articleId']; ?>" method="post">
      <div class="modal-body">
        La date actuelle est : <?= $vart->getReg_date(); ?>
        <br /><br />
        	<div class="form-group">
        		<label for="articles_date">Modifier :</label> <input type="date" class="form-control" id="articles_date" placeholder="jj-mm-aaaa" name="real_date">
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" name="sendEditDate" class="btn btn-primary">Sauvegarder</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="titleEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modification du titre</h4>
      </div>
      <form name="editTitle" action="view/single_article/edit_articles.php?id=<?= $_GET['articleId']; ?>" method="post">
      <div class="modal-body">
      	<div class="form-group">
        	<label for="title">Titre :</label><textarea class="form-control" id="title" rows="4" name="title"><?= $vart->getTitle(); ?></textarea>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" name="sendEditTitle" class="btn btn-primary">Sauvegarder</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="removeAll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Supprimer la page</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" role="alert"><b>Attention !</b> Êtes-vous sûr de vouloir supprimer définitivement cette page ?</div>
      </div>
      <form name="supprPage" action="view/single_article/edit_articles.php?id=<?= $_GET['articleId']; ?>" method="post">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-danger" name="sendSuppr">Confirmer la suppression</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- End modals -->
<?php
	}
}
?>
<div class="panel panel-default">
	<div class="panel-heading"><h3 style="text-align: center; font-weight: 800;"><?= $vart->getTitle(); ?></h3></div>
<?php
if (isset($_GET['edit']) && $_GET['edit'] == 1)
{
	if ($_SESSION['id'] == 1 || $_SESSION['id'] == 2)
	{
?>
<form method="post" action="view/single_article/edit_articles.php?id=<?= $_GET['articleId']; ?>" enctype="multipart/form-data">
  <div class="form-group">
    <textarea class="form-control" id="articles_body" placeholder="Tapez l'article" rows="4" name="article_description"><?= $vart->getBodytext(); ?></textarea>
  </div>
  <button type="submit" class="btn btn-primary" name="article_sending">Sauvegarder</button>
  <button type="submit" class="btn btn-default" name="cancelBody">Annuler</button>
</form>
<script>CKEDITOR.replace('article_description', {
	filebrowserBrowseUrl: '/articles_pics/sel_pics.php',
    filebrowserUploadUrl: '/articles_pics/upload.php'
});</script>
<?php
	}
	else
	{
		echo '<div class="panel-body">'.html_entity_decode($vart->getBodytext(), ENT_HTML5 | ENT_QUOTES).'</div>';
	}
}
else
{
	echo '<div class="panel-body">'.html_entity_decode($vart->getBodytext(), ENT_HTML5 | ENT_QUOTES).'</div>';
}
?>
</div>
<p class="text-right"><small>Auteur : <span style="text-decoration: underline;"><?= $vart->getAuthor(); ?></span></small></p>