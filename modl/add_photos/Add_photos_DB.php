<?php
class Add_photos_DB
{
	public function __construct($post, $file_name)
	{
		include '../../modl/DBFactory.php';
			include '../../modl/UserManagerPDO.php';
			$db = DBFactory::getPDOCon();
			$manager = new UserManagerPDO($db);
			$conx = $manager->getDB();
			$req = $conx->prepare('INSERT INTO mygen_photos(id_member, picsname, reg_date, description, type) VALUES(:id_member, :picsname, :reg_date, :description, :type)');
			$req->execute(array(':id_member' => 1,
					':picsname' => $file_name,
					':reg_date' => $post['real_date'],
					':description' => htmlspecialchars($post['description']),
					':type' => 'photo'
			));
			session_start();
			$_SESSION['success'] = '<div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-ok" style="vertical-align:text-top"></span> Photo ajoutée avec succès !</div>';
			header('Location: '.$_SERVER['HTTP_REFERER']);
	}
}