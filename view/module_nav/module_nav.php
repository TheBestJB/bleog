<nav class="navbar navbar-default navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Bleog</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
<?php
$active = $_SERVER['REQUEST_URI'] == '/Accueil.php' ? ' class="active"' : '';
$active_AP = $_SERVER['REQUEST_URI'] == '/add_photos.php' ? ' class="active"' : '';
$active_AV = $_SERVER['REQUEST_URI'] == '/add_videos.php' ? ' class="active"' : '';
$active_AA = $_SERVER['REQUEST_URI'] == '/add_articles.php' ? ' class="active"' : '';
$active_AN = $_SERVER['REQUEST_URI'] == '/newsletter.php' ? ' class="active"' : '';
?>
              <li<?= $active ?>><a href="index.php"><span class="glyphicon glyphicon-home"></span> Accueil</a></li>
<?php 
if ($_SESSION['id'] == 1 || $_SESSION['id'] == 2)
{
?>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                 <span class="glyphicon glyphicon-menu-hamburger"></span> Ajouts Admin <span class="caret"></span>
                </a>
                  <ul class="dropdown-menu">
                    <li<?= $active_AP ?>><a href="add_photos.php"><span class="glyphicon glyphicon-camera"></span> Ajout photos</a></li>
                    <li<?= $active_AV ?>><a href="add_videos.php"><span class="glyphicon glyphicon-film"></span> Ajout vidéos</a></li>
                    <li<?= $active_AA ?>><a href="add_articles.php"><span class="glyphicon glyphicon-pencil"></span> Ajout articles</a></li>
                    <li<?= $active_AN ?>><a href="newsletter.php"><span class="glyphicon glyphicon-envelope"></span> Envoi newsletters</a></li>
                  </ul>
              </li>
<?php 
}
?>
              <li>
                <a href="/articles.php?articleId=25">
                <span class="glyphicon glyphicon-tree-conifer"></span> <b><font color="blue">Origine du prénom</font></b></a>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
            	<li><a href="deco.php"><span class="glyphicon glyphicon-remove" style="vertical-align:text-top"></span> Déconnexion</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>