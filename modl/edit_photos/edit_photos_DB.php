<?php
require_once '../../modl/DBFactory.php';
/**
 * class edit_photos_DB
 */
class edit_photos_DB
{
	/**
	 * Instance PDO
	 */
	protected $db;
	
	/**
	 * Connexion à la BDD
	 * 
	 * Constructeur
	 */
	public function __construct()
	{
		$db = DBFactory::getPDOCon();
		$this->db = $db;
	}
	
	/**
	 * Supprime la description N° id
	 * 
	 * @param int $id
	 */
	public function suppDescr($id)
	{
		$req = $this->db->prepare('UPDATE mygen_photos SET description = :description WHERE id = :id');
		$req->execute(array(':description' => NULL, ':id' => $id));
	}
	
	/**
	 * Modifie la description
	 * 
	 * @param int $id
	 * @param string $desc
	 */
	public function modifDesc($id, $desc)
	{
		$req = $this->db->prepare('UPDATE mygen_photos SET description = :description WHERE id = :id');
		$req->execute(array(':description' => $desc, ':id' => $id));
	}
	
	/**
	 * Modifie la date en bdd
	 * 
	 * @param int $id
	 * @param date $date
	 */
	public function modifDate($id, $date)
	{
		$req = $this->db->prepare('UPDATE mygen_photos SET reg_date = :reg_date WHERE id = :id');
		$req->execute(array(':reg_date' => $date, ':id' => $id));
	}
	
	/**
	 * Supprime l'ancienne photo et modifie le picsname
	 * 
	 * @param int $id
	 * @param string $file
	 */
	public function modifPhoto($id, $file)
	{
		$this->delPhoto($id);
		
		$req = $this->db->prepare('UPDATE mygen_photos SET picsname = :picsname WHERE id = :id');
		$req->execute(array(':picsname' => $file, ':id' => $id));
	}
	
	/**
	 * Supprime l'entrée N°id puis supprime les fichiers
	 * 
	 * @param int $id
	 */
	public function supprPage($id)
	{
		$this->delPhoto($id);
		
		$req = $this->db->prepare('DELETE FROM mygen_photos WHERE id = :id');
		$req->execute(array(':id' => $id));
	}
	
	/**
	 * Supprime la photo dans pics et pics/mini
	 * 
	 * @param int $id
	 */
	public function delPhoto($id)
	{
		$selOldPics = $this->db->prepare('SELECT picsname FROM mygen_photos WHERE id = :id');
		$selOldPics->execute(array(':id' => $id));
		$oldPics = $selOldPics->fetch();
		unlink('../../pics/'.$oldPics['picsname']);
		unlink('../../pics/mini/'.$oldPics['picsname']);
	}
}