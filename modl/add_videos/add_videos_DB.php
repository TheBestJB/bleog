<?php
require_once '../../modl/DBFactory.php';
require_once '../../modl/UserManagerPDO.php';

/**
 * 
 * Classe d'ajout de vidéo en db
 *
 */
class add_videos_DB
{
	/**
	 * Constructeur
	 * 
	 * @param date $date
	 * @param string $title
	 * @param string $url
	 * @return bool
	 */
	public function __construct($date, $title, $url)
	{
		$db = DBFactory::getPDOCon();
		$manager = new UserManagerPDO($db);
		$conx = $manager->getDB();
		$req = $conx->prepare('INSERT INTO mygen_videos(id_member, url_title, url_link, type, reg_date) VALUES(:id_member, :url_title, :url_link, :type, :reg_date)');
		$req->execute(array(
				':id_member' => 1,
				':url_title' => $title,
				':url_link' => $url,
				':type' => 'video',
				':reg_date' => $date
		));
		return true;
	}
}